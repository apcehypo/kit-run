{$DEFINE UNICODE_CTRLS}
{$I KOLDEF.inc}
unit runData;

interface

uses KOL, XIniFile, CommonTools, Context, OSTools{$IFDEF DEBUG}, TypInfo, SysUtils{$ENDIF};

type
  TPathVariant = (Path_WorkDir, Path_StartDir, Path_ExecDir);
  TExecTypeVariant = (ExecType_None, ExecType_Single, ExecType_ExecList, ExecType_Directory);
  TWaitVariant = (Wait_None, Wait_AnyExit, Wait_AllExit, Wait_Number, Wait_Command, Wait_Time);
  // TElevateVariant = (Elevate_None, Elevate_User, Elevate_Highest, Elevate_Admin);
  TCheckVariant = (Check_None, Check_KeyAbsent, Check_FirstKeyAbsent, Check_AnyKeyAbsent, Check_AllKeyAbsent);
  TRegAccessVariant = (RegAccess_None, RegAccess_Keys, RegAccess_Hives);
  TRegImportVariant = (RegImport_Single, RegImport_RegsList, RegImport_Directory, RegImport_Embedded, RegImport_None);
  TOnCommandVariant = (OnCommand_None, OnCommand_Close, OnCommand_Terminate);

  PRunData = ^TRunData;

  TRunData = object(TObj)
  private
    function GetBaseDir: KOLString;
    procedure SetRelativePaths(Value: KOLString);
    procedure SetWorkDir(Value: KOLString);
    procedure SetExecType(Value: KOLString);
    procedure SetWait(Value: KOLString);
    procedure SetOnCommand(Value: KOLString);
    procedure SetCheck(Value: KOLString);
    procedure SetRestore(Value: KOLString);
    procedure SetBackup(Value: KOLString);
    procedure SetDelete(Value: KOLString);
    procedure SetImport(Value: KOLString);
    procedure SetSave(Value: KOLString);
    procedure SetUnregister(Value: KOLString);
  public
    RunFileName: KOLString;
    Context: PContext;

    // 2.1) ������ [Configuration]
    RelativePaths: TPathVariant;
    WorkDir: TPathVariant;
    ExecType: TExecTypeVariant;
    Execute: KOLString;
    Wait: TWaitVariant;
    WaitValue: Cardinal;
    Instance: KOLString;
    // Elevate: TElevateVariant;
    OnCommand: TOnCommandVariant;

    // 2.2) ������ [Registry]
    Check: TCheckVariant;
    Key: KOLString;
    Restore: TRegAccessVariant;
    Backup: TRegAccessVariant;
    BackupFile: KOLString;
    Delete: TRegAccessVariant;
    Import: TRegImportVariant;
    RegSource: KOLString;
    Save: TRegAccessVariant;
    SaveFile: KOLString;
    Unregister: TRegAccessVariant;

    // 2.3) ������ [ExecList]
    ExecList: PKOLStrList;

    // 2.4) ������[RegsList]
    RegsList: PKOLStrList;

    // 2.5) ������[Variables]
    Variables: PKOLStrList;

    // 2.6) ������[Environment]
    Environment: PKOLStrList;

    // 2.7) �������� ��������
    PreCopyFiles: PKOLStrList;
    PostCopyFiles: PKOLStrList;
    PreRemoveFiles: PKOLStrList;
    PostRemoveFiles: PKOLStrList;

    // 2.8) ������ ������
    OtherSections: PKOLStrListEx;

    property BaseDir: KOLString read GetBaseDir;
    procedure DefineBaseDir;
    procedure ResolveRelativePath(var path: KOLString);
    function ResolvedRelativePath(const path: KOLString): KOLString;
  end;

function InitRunData: PRunData;
function GetRunData(runData: PXIniFile): PRunData;

{$IFDEF DEBUG}
procedure PrintRunData(data: PRunData);
{$ENDIF}

implementation

{$IFDEF DEBUG}

procedure PrintRunData(data: PRunData);
var
  text: KOLString;
  i: Integer;
begin
  text := '[Configuration]' + #13#10;
  text := text + 'RelativePaths=' + GetEnumName(TypeInfo(TPathVariant), Integer(data.RelativePaths)) + #13#10;
  text := text + 'WorkDir=' + GetEnumName(TypeInfo(TPathVariant), Integer(data.WorkDir)) + #13#10;
  text := text + 'ExecType=' + GetEnumName(TypeInfo(TExecTypeVariant), Integer(data.ExecType)) + #13#10;
  text := text + 'Execute=' + data.Execute + #13#10;
  text := text + 'Wait=' + GetEnumName(TypeInfo(TWaitVariant), Integer(data.Wait)) + #13#10;
  text := text + 'WaitValue=' + Int2Str(data.WaitValue) + #13#10;
  text := text + 'Instance=' + data.Instance + #13#10;
  text := text + 'OnCommand=' + GetEnumName(TypeInfo(TOnCommandVariant), Integer(data.RelativePaths)) + #13#10;

  text := text + '[Registry]' + #13#10;
  text := text + 'Check=' + GetEnumName(TypeInfo(TCheckVariant), Integer(data.Check)) + #13#10;
  text := text + 'Key=' + data.Key + #13#10;
  text := text + 'Restore=' + GetEnumName(TypeInfo(TRegAccessVariant), Integer(data.Restore)) + #13#10;
  text := text + 'Backup=' + GetEnumName(TypeInfo(TRegAccessVariant), Integer(data.Backup)) + #13#10;
  text := text + 'BackupFile=' + data.BackupFile + #13#10;
  text := text + 'Delete=' + GetEnumName(TypeInfo(TRegAccessVariant), Integer(data.Delete)) + #13#10;
  text := text + 'Import=' + GetEnumName(TypeInfo(TRegImportVariant), Integer(data.Import)) + #13#10;
  text := text + 'RegSource=' + data.RegSource + #13#10;
  text := text + 'Save=' + GetEnumName(TypeInfo(TRegAccessVariant), Integer(data.Save)) + #13#10;
  text := text + 'SaveFile=' + data.SaveFile + #13#10;
  text := text + 'Unregister=' + GetEnumName(TypeInfo(TRegAccessVariant), Integer(data.Unregister)) + #13#10;

  text := text + '[ExecList]' + #13#10;
  for i := 0 to data.ExecList.Count - 1 do
    text := text + data.ExecList.Items[i] + #13#10;

  text := text + '[RegsList]' + #13#10;
  for i := 0 to data.RegsList.Count - 1 do
    text := text + data.RegsList.Items[i] + #13#10;

  text := text + '[PreCopyFiles]' + #13#10;
  for i := 0 to data.PreCopyFiles.Count - 1 do
    text := text + data.PreCopyFiles.Items[i] + #13#10;

  text := text + '[PostCopyFiles]' + #13#10;
  for i := 0 to data.PostCopyFiles.Count - 1 do
    text := text + data.PostCopyFiles.Items[i] + #13#10;

  text := text + '[PreRemoveFiles]' + #13#10;
  for i := 0 to data.PreRemoveFiles.Count - 1 do
    text := text + data.PreRemoveFiles.Items[i] + #13#10;

  text := text + '[PostRemoveFiles]' + #13#10;
  for i := 0 to data.PostRemoveFiles.Count - 1 do
    text := text + data.PostRemoveFiles.Items[i] + #13#10;

  text := text + '[Variables]' + #13#10;
  for i := 0 to data.Variables.Count - 1 do
    text := text + data.Variables.Items[i] + #13#10;

  text := text + '[Environment]' + #13#10;
  for i := 0 to data.Environment.Count - 1 do
    text := text + data.Environment.Items[i] + #13#10;

  text := text + '> BaseDir=' + data.BaseDir + #13#10;
  echo(text);
end;
{$ENDIF}

function InitRunData: PRunData;
begin
  New(Result, Create);
{$IFDEF DEBUG_OBJKIND}
  Result.fObjKind := 'TRunData';
{$ENDIF}
  Result.RelativePaths := Path_WorkDir;
  Result.WorkDir := Path_WorkDir;
  Result.ExecType := ExecType_None;
  Result.Wait := Wait_None;
  Result.OnCommand := OnCommand_None;

  Result.Check := Check_FirstKeyAbsent;
  Result.Restore := RegAccess_None;
  Result.Backup := RegAccess_None;
  Result.Delete := RegAccess_None;
  Result.Import := RegImport_Embedded;
  Result.Save := RegAccess_None;
  Result.Unregister := RegAccess_None;

  Result.ExecList := NewKOLStrList;
  Result.RegsList := NewKOLStrList;
  Result.PreCopyFiles := NewKOLStrList;
  Result.PostCopyFiles := NewKOLStrList;
  Result.PreRemoveFiles := NewKOLStrList;
  Result.PostRemoveFiles := NewKOLStrList;
  Result.Variables := NewKOLStrList;
  Result.Environment := NewKOLStrList;
end;

function GetRunData(runData: PXIniFile): PRunData;
var
  i: Integer;
  temp: KOLString;
  data: PKOLStrList;
  name, value: KOLString;
begin
  Result := InitRunData;
  Result.RunFileName := runData.FileName;
  runData.Mode := ifmRead;
  Result.Context := InitContext;

  //������ ������ Variables � Environment
  Result.Context.Vars['WorkDir'] := GetWorkDir;
  Result.Context.Vars['StartDir'] := GetStartDir;
  Result.Context.Vars['UserName'] := UserName;
  //!!!Result.Context.Vars['UserHive'] :=
  Result.Context.Vars['ArchBits'] := IfThenElseStr('64', '32', IsWOW64);

  // 2.5) ������ [Variables]
  runData.Section := 'Variables';
  data := runData.GetSectionData;
  for i := 1 to runData.KeysCount - 1 do
  begin
    value := data.Items[i];
    name := Parse(value, '=');
    Result.Context.Resolve(value);
    Result.Context.Vars[name] := value;
  end;
  Result.Context.Vars['WorkDir'] := IncludeTrailingPathDelimiter(Result.Context.Vars['WorkDir']);
  Result.Context.Vars['StartDir'] := IncludeTrailingPathDelimiter(Result.Context.Vars['StartDir']);

  // 2.6) ������ [Environment]
  runData.Section := 'Environment';
  data := runData.GetSectionData;
  for i := 1 to runData.KeysCount - 1 do
  begin
    value := data.Items[i];
    if (IndexOfStr(value, '=') > 0) then
    begin
      name := Parse(value, '=');
      Result.Context.Resolve(value);
      Result.Context.Envs[name] := value;
    end
    else
    begin //�������� ����������
      SetEnvironmentVariable(PKOLChar(value), nil);
    end;
  end;
  
  // 2.1) ������ [Configuration]
  runData.Section := 'Configuration';
  // 2.1.1) RelativePaths
  Result.SetRelativePaths(runData.ValueString('RelativePaths', #13));
  // 2.1.2) WorkDir
  Result.SetWorkDir(runData.ValueString('WorkDir', #13));
  // 2.1.3) ExecType
  temp := runData.ValueString('ExecType', #13);
  Result.SetExecType(temp);
  // �� ���������: ����������. ���� ����� ���� Execute, �� �ExecType=Single�, ����� �ExecType=None�.
  // 2.1.4) Execute
  Result.Execute := runData.ValueString('Execute', #13);
  if (Result.Execute <> #13) and (temp = #13) then
    Result.ExecType := ExecType_Single;
  if (Result.ExecType = ExecType_Directory) then
    IncludeTrailingPathDelimiter(Result.Execute);
  //!!! VARIABLES
  // 2.1.6) Instance
  Result.Instance := runData.ValueString('Instance', #13);
  if (Result.Instance = '') then
  begin
    warn('Instance=');
  end
  else
    if (Result.Instance = #13) then
    begin
      Result.Instance := '';
    end;
  // 2.1.7) Elevate  // 2.1.7.1) None  // 2.1.7.2) User  // 2.1.7.3) Highest  // 2.1.7.4) Admin  // �� ���������: None.
  // 2.1.8) OnCommand
  Result.SetOnCommand(runData.ValueString('OnCommand', #13));

  // 2.2) ������ [Registry]
  runData.Section := 'Registry';
  // 2.2.1) Check
  Result.SetCheck(runData.ValueString('Check', #13));
  // 2.2.2) Key
  if (Result.Check = Check_KeyAbsent) then
  begin
    Result.Key := runData.ValueString('Key', #13);
    if (Result.Key = #13) then
    begin
      Result.Key := '';
      warn('Key=');
    end;
  end;
  // 2.2.3) Restore
  Result.SetRestore(runData.ValueString('Restore', #13));
  // 2.2.4) Backup
  Result.SetBackup(runData.ValueString('Backup', #13));
  // 2.2.5) BackupFile
  // 2.2.5.1) ���� BackupFile �� ����� � Import=Single
  // 2.2.5.2) ���� BackupFile �� ����� � Import=RegsList
  // 2.2.5.3) ���� BackupFile �� ����� � Import=Directory
  // 2.2.5.4) ���� BackupFile �� ����� � Import=Embedded
  // 2.2.6) Delete
  Result.SetDelete(runData.ValueString('Delete', #13));
  // 2.2.7) Import
  Result.SetImport(runData.ValueString('Import', #13));
  // 2.2.8) RegSource
  Result.RegSource := runData.ValueString('RegSource', #13);
  if ((Result.Import = RegImport_Single) or (Result.Import = RegImport_Directory)) and (Result.RegSource = #13) then
  begin
    warn('RegSource=');
    Result.RegSource := '';
    Result.Import := RegImport_None;
  end;
  // 2.2.8.1) ���� Import=Single
  // 2.2.8.2) ���� Import=Directory
  // 2.2.9) Save
  Result.SetSave(runData.ValueString('Save', #13));
  // 2.2.10) SaveFile
  // 2.2.10.1) ���� SaveFile �� ����� � Import=Single
  // 2.2.10.2) ���� SaveFile �� ����� � Import=RegsList
  // 2.2.10.3) ���� SaveFile �� ����� � Import=Directory
  // 2.2.10.4) ���� SaveFile �� ����� � Import=Embedded
  // 2.2.11) Unregister
  Result.SetUnregister(runData.ValueString('Unregister', #13));

  // 2.3) ������ [ExecList]
  if (Result.ExecType = ExecType_ExecList) then
  begin
    runData.Section := 'ExecList';
    data := runData.GetSectionData;
    for i := 1 to runData.KeysCount - 1 do
    begin
      Result.ExecList.Add(data.Items[i]);
    end;
  end;

  // 2.1.5) Wait
  runData.Section := 'Configuration';
  temp := runData.ValueString('Wait', #13);
  // �� ���������: ����������. ���� ���������� ����� �� �������� Unregister, Restore � Save, �� �Wait=1�, ����� �Wait=None�.
  if (temp = #13) and ((Result.Unregister <> RegAccess_None) or (Result.Restore <> RegAccess_None) or (Result.Save <> RegAccess_None)) then
  begin
    Result.Wait := Wait_Number;
    Result.WaitValue := 1;
  end
  else
    if (temp <> #13) then
    begin
      Result.SetWait(temp);
      if (Result.Wait = Wait_Command) and (Result.Instance = '') then
      begin
        warn('NO Instance= FOR Wait=Command');
        Result.Wait := Wait_None;
      end;
    end;

  // 2.4) ������ [RegsList]
  if (Result.Import = RegImport_RegsList) then
  begin
    runData.Section := 'RegsList';
    data := runData.GetSectionData;
    for i := 1 to runData.KeysCount - 1 do
    begin
      Result.RegsList.Add(data.Items[i]);
    end;
  end;

  // 2.7) �������� ��������
  // 2.7.1) ������ [PreCopyFiles]
  runData.Section := 'PreCopyFiles';
  data := runData.GetSectionData;
  for i := 1 to runData.KeysCount - 1 do
  begin
    Result.PreCopyFiles.Add(data.Items[i]);
    //!!! VARIABLES
  end;
  // 2.7.2) ������ [PostCopyFiles]
  runData.Section := 'PostCopyFiles';
  data := runData.GetSectionData;
  for i := 1 to runData.KeysCount - 1 do
  begin
    Result.PostCopyFiles.Add(data.Items[i]);
    //!!! VARIABLES
  end;
  // 2.7.3) ������ [PreRemoveFiles]
  runData.Section := 'PreRemoveFiles';
  data := runData.GetSectionData;
  for i := 1 to runData.KeysCount - 1 do
  begin
    Result.PreRemoveFiles.Add(data.Items[i]);
    //!!! VARIABLES
  end;
  // 2.7.4) ������ [PostRemoveFiles]
  runData.Section := 'PostRemoveFiles';
  data := runData.GetSectionData;
  for i := 1 to runData.KeysCount - 1 do
  begin
    Result.PostRemoveFiles.Add(data.Items[i]);
    //!!! VARIABLES
  end;

  // 2.8) ������ ������
  if (Result.Import = RegImport_Embedded) then
  begin
    runData.DeleteSection('Configuration');
    runData.DeleteSection('Registry');
    runData.DeleteSection('ExecList');
    runData.DeleteSection('RegsList');
    runData.DeleteSection('PreRemoveFiles');
    runData.DeleteSection('PreCopyFiles');
    runData.DeleteSection('PostCopyFiles');
    runData.DeleteSection('PostRemoveFiles');
    runData.DeleteSection('Variables');
    runData.DeleteSection('Environment');
    Result.OtherSections := runData.Sections;
    runData.Sections := nil;
  end;
end;

procedure TRunData.DefineBaseDir;
var
  exec: KOLString;
label
  BAD_EXEC;
begin
  case Self.RelativePaths of
    Path_WorkDir: Context.BaseDir := Context.Vars['WorkDir'];
    Path_StartDir: Context.BaseDir := Context.Vars['StartDir'];
    Path_ExecDir:
      begin
        case Self.ExecType of
          ExecType_Single:
            exec := Self.Execute;
          ExecType_ExecList:
            if (Self.ExecList.Count > 0) then
              exec := Self.ExecList.Items[0]
            else
              goto BAD_EXEC;
          ExecType_None, ExecType_Directory:
            goto BAD_EXEC;
        end;
        //!!!
        if IsAbsolutePath(exec) then
          Context.BaseDir := ExtractFilePath(exec)
        else
          goto BAD_EXEC;
      end;
  end;
  Exit;
  BAD_EXEC:
  warn('RelativePaths CONFLICTS WITH ExecType');
end;

function TRunData.GetBaseDir: KOLString;
begin
  Result := Self.Context.BaseDir;
end;

procedure TRunData.SetRelativePaths(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.1.1.1) WorkDir
  if (UpperValue = 'WORKDIR') then
    RelativePaths := Path_WorkDir
  else
    // 2.1.1.2) StartDir
    if (UpperValue = 'STARTDIR') then
      RelativePaths := Path_StartDir
    else
      // 2.1.1.3) Exe�Dir
      if (UpperValue = 'EXECDIR') then
        RelativePaths := Path_ExecDir
      else
      begin
        // �� ���������: WorkDir
        RelativePaths := Path_WorkDir;
        if (Value <> #13) then
          warn('RelativePaths=' + Value);
      end;
end;

procedure TRunData.SetWorkDir(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.1.2.1) WorkDir
  if (UpperValue = 'WORKDIR') then
    WorkDir := Path_WorkDir
  else
    // 2.1.2.2) StartDir
    if (UpperValue = 'STARTDIR') then
      WorkDir := Path_StartDir
    else
      // 2.1.2.3) Exe�Dir
      if (UpperValue = 'EXECDIR') then
        WorkDir := Path_ExecDir
      else
      begin
        // �� ���������: WorkDir
        WorkDir := Path_WorkDir;
        if (Value <> #13) then
          warn('WorkDir=' + Value);
      end;
end;

procedure TRunData.SetExecType(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.1.3.1) None
  if (UpperValue = 'NONE') then
    ExecType := ExecType_None
  else
    // 2.1.3.2) Single
    if (UpperValue = 'SINGLE') then
      ExecType := ExecType_Single
    else
      // 2.1.3.3) ExecList
      if (UpperValue = 'EXECLIST') then
        ExecType := ExecType_ExecList
      else
        // 2.1.3.4) Directory
        if (UpperValue = 'DIRECTORY') then
          ExecType := ExecType_Directory
        else
        begin
          // �� ���������: None
          ExecType := ExecType_None;
          if (Value <> #13) then
            warn('ExecType=' + Value);
        end;
end;

procedure TRunData.SetWait(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // �� ���������: None
  Wait := Wait_None;
  // 2.1.5.1) None
  if (UpperValue = 'NONE') then
    Wait := Wait_None
  else
    // 2.1.5.2) AnyExit
    if (UpperValue = 'ANYEXIT') then
      Wait := Wait_AnyExit
    else
      // 2.1.5.3) AllExit
      if (UpperValue = 'ALLEXIT') then
        Wait := Wait_AllExit
      else
        // 2.1.5.5) Command
        if (UpperValue = 'COMMAND') then
          Wait := Wait_Command
        else
        begin
          if (Length(UpperValue) <> 0) then
          begin
            if (Value <> #13) then
            begin
              // 2.1.5.4) <�����>
              if (IsDigit(UpperValue[1])) then
              begin
                Wait := Wait_Number;
                WaitValue := Str2Int(UpperValue) - 1;
                if (WaitValue < 1) or (WaitValue >= ExecList.Count) then
                begin
                  WaitValue := 0;
                  warn('Wait= OUT OF RANGE');
                end;  
              end
              else
                if (Length(UpperValue) > 1) then
                begin
                  if (UpperValue[1] = '-') and (IsDigit(UpperValue[2])) then
                  begin
                    Wait := Wait_Time;
                    WaitValue := Cardinal(0 - Str2Int(UpperValue));
                  end;
                end
                else
                  warn('Wait=' + Value);
            end;
          end
          else
            warn('Wait=');
        end;
end;

procedure TRunData.SetOnCommand(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.1.8.1) None
  if (UpperValue = 'NONE') then
    OnCommand := OnCommand_None
  else
    // 2.1.8.2) Close
    if (UpperValue = 'CLOSE') then
      OnCommand := OnCommand_Close
    else
      // 2.1.8.3) Terminate
      if (UpperValue = 'TERMINATE') then
        OnCommand := OnCommand_Terminate
      else
      begin
        OnCommand := OnCommand_None;
        if (Value <> #13) then
          warn('OnCommand=' + Value);
      end;
end;

procedure TRunData.SetCheck(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.2.1.1) No
  if (UpperValue = 'NONE') then
    Check := Check_None
  else
    // 2.2.1.2) KeyAbsent
    if (UpperValue = 'KEYABSENT') then
      Check := Check_KeyAbsent
    else
      // 2.2.1.3) FirstKeyAbsent
      if (UpperValue = 'FIRSTKEYABSENT') then
        Check := Check_FirstKeyAbsent
      else
        // 2.2.1.4) AnyKeyAbsent
        if (UpperValue = 'ANYKEYABSENT') then
          Check := Check_AnyKeyAbsent
        else
          // 2.2.1.5) AllKeyAbsent
          if (UpperValue = 'ALLKEYABSENT') then
            Check := Check_AllKeyAbsent
          else
          begin
            // �� ���������: FirstKeyAbsent
            Check := Check_FirstKeyAbsent;
            if (Value <> #13) then
              warn('Check=' + Value);
          end;
end;

procedure TRunData.SetRestore(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.2.3.1) None
  if (UpperValue = 'NONE') then
    Restore := RegAccess_None
  else
    // 2.2.3.2) Keys
    if (UpperValue = 'KEYS') then
      Restore := RegAccess_Keys
    else
      // 2.2.3.3) Hives
      if (UpperValue = 'HIVES') then
        Restore := RegAccess_Hives
      else
      begin
        // �� ���������: None
        Restore := RegAccess_None;
        if (Value <> #13) then
          warn('Restore=' + Value);
      end;
end;

procedure TRunData.SetBackup(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.2.4.1) None
  if (UpperValue = 'NONE') then
    Backup := RegAccess_None
  else
    // 2.2.4.2) Keys
    if (UpperValue = 'KEYS') then
      Backup := RegAccess_Keys
    else
      // 2.2.4.3) Hives
      if (UpperValue = 'HIVES') then
        Backup := RegAccess_Hives
      else
      begin
        // �� ���������: None
        Backup := RegAccess_None;
        if (Value <> #13) then
          warn('Backup=' + Value);
      end;
end;

procedure TRunData.SetDelete(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.2.6.1) None
  if (UpperValue = 'NONE') then
    Delete := RegAccess_None
  else
    // 2.2.6.2) Keys
    if (UpperValue = 'KEYS') then
      Delete := RegAccess_Keys
    else
      // 2.2.6.3) Hives
      if (UpperValue = 'HIVES') then
        Delete := RegAccess_Hives
      else
      begin
        // �� ���������: None
        Restore := RegAccess_None;
        if (Value <> #13) then
          warn('Delete=' + Value);
      end;
end;

procedure TRunData.SetImport(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.2.7.1) Single
  if (UpperValue = 'SINGLE') then
    Import := RegImport_Single
  else
    // 2.2.7.2) RegsList
    if (UpperValue = 'REGSLIST') then
      Import := RegImport_RegsList
    else
      // 2.2.7.3) Directory
      if (UpperValue = 'DIRECTORY') then
        Import := RegImport_Directory
      else
        // 2.2.7.4) Embedded
        if (UpperValue = 'EMBEDDED') then
          Import := RegImport_Embedded
        else
          // 2.2.7.5) None
          if (UpperValue = 'NONE') then
            Import := RegImport_None
          else
          begin
            // �� ���������: Embedded
            Import := RegImport_Embedded;
            if (Value <> #13) then
              warn('Import=' + Value);
          end;
end;

procedure TRunData.SetSave(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.2.9.1) None
  if (UpperValue = 'NONE') then
    Save := RegAccess_None
  else
    // 2.2.9.2) Keys
    if (UpperValue = 'KEYS') then
      Save := RegAccess_Keys
    else
      // 2.2.9.3) Hives
      if (UpperValue = 'HIVES') then
        Save := RegAccess_Hives
      else
      begin
        // �� ���������: None
        Save := RegAccess_None;
        if (Value <> #13) then
          warn('Save=' + Value);
      end;
end;

procedure TRunData.SetUnregister(Value: KOLString);
var
  UpperValue: KOLString;
begin
  UpperValue := UpperCase(Value);
  // 2.2.11.1) None
  if (UpperValue = 'NONE') then
    Unregister := RegAccess_None
  else
    // 2.2.11.2) Keys
    if (UpperValue = 'KEYS') then
      Unregister := RegAccess_Keys
    else
      // 2.2.11.3) Hives
      if (UpperValue = 'HIVES') then
        Unregister := RegAccess_Hives
      else
      begin
        // �� ���������: None
        Unregister := RegAccess_None;
        if (Value <> #13) then
          warn('Unregister=' + Value);
      end;
end;

procedure TRunData.ResolveRelativePath(var path: KOLString);
begin
  path := ResolvedRelativePath(path);
end;

function TRunData.ResolvedRelativePath(const path: KOLString): KOLString;
begin
  if not IsAbsolutePath(path) then
    Result := Self.BaseDir + path
  else
    Result := path;
end;

end.

