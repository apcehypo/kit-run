{$DEFINE UNICODE_CTRLS}
{$I KOLDEF.inc}
unit RegModel;

interface

uses Windows, KOL, XIniFile, CommonTools, Registry, RunData;

type
  PRegData = ^TRegData;
  TRegData = object(TObj)
  public Files: PKOLStrListEx; //[name:PRegFile]*
  public function CloneAndGetDataFromRegistry(useGeneralKeys: Boolean): PRegData;
  public procedure ImportKeysToRegistry;
  public procedure DeleteKeysFromRegistry(useGeneralKeys: Boolean);
  end;

function GetEmptyRegData(): PRegData;
function GetRegDataFromSingleFile(fileName: KOLString): PRegData;
function GetRegDataFromRegsList(list: PKOLStrList): PRegData;
function GetRegDataFromDirectory(dirName: KOLString): PRegData;
function GetEmbeddedRegData(runData: PRunData; runFile: KOLString): PRegData;

type
  PRegFile = ^TRegFile;
  TRegFile = object(TObj)
  public Sections: PKOLStrListEx; //[header:PKOLStrList]*
  public AllKeys: PKOLStrList;
  public GeneralKeys: PKOLStrList;
  public procedure CollectKeys;
  end;

function ParseFile(fileName: KOLString): PRegFile;
function ParseRegSections(sections: PKOLStrListEx): PRegFile;
function GetEmptyRegFile(): PRegFile;
function ReadRegData(run: PRunData): PRegData;

{$IFDEF DEBUG}
procedure PrintRegData(data: PRegData);
{$ENDIF}

implementation

{$IFDEF DEBUG}

procedure PrintRegData(data: PRegData);
var
  text: KOLString;
  iFile, iSect, iParam: Integer;
  regFile: PRegFile;
  sect: PKOLStrList;
begin
  text := '';
  for iFile := 0 to data.Files.Count - 1 do
  begin
    text := text + #13#10 + '=====> FILE: ' + data.Files.Items[iFile] + #13#10;
    regFile := PRegFile(data.Files.Objects[iFile]);
    for iSect := 0 to regFile.Sections.Count - 1 do
    begin
      text := text + #13#10 + '=> [' + regFile.Sections.Items[iSect] + ']' + #13#10;
      sect := PKOLStrList(regFile.Sections.Objects[iSect]);
      for iParam := 0 to sect.Count - 1 do
      begin
        text := text + '> ' + sect.Items[iParam] + #13#10;
      end;
    end;
    if (regFile.AllKeys <> nil) then
    begin
      text := text + #13#10 + ' ~~~> AllKeys: ' + #13#10;
      for iSect := 0 to regFile.AllKeys.Count - 1 do
      begin
        text := text + ' ~> [' + regFile.AllKeys.Items[iSect] + ']' + #13#10;
      end;
    end;
    if (regFile.GeneralKeys <> nil) then
    begin
      text := text + #13#10 + ' ~~~> GeneralKeys: ' + #13#10;
      for iSect := 0 to regFile.GeneralKeys.Count - 1 do
      begin
        text := text + ' ~> [' + regFile.GeneralKeys.Items[iSect] + ']' + #13#10;
      end;
    end;
  end;
  echo(text);
end;
{$ENDIF}

function ReadRegData(run: PRunData): PRegData;
var
  i: Integer;
  value: KOLString;
begin
  case run.Import of
    RegImport_Single:
      begin
        run.ResolveRelativePath(run.RegSource);
        Result := GetRegDataFromSingleFile(run.RegSource);
      end;
    RegImport_RegsList:
      begin
        for i := 0 to run.RegsList.Count - 1 do
        begin
          value := run.RegsList.Items[i];
          run.ResolveRelativePath(value);
          run.RegsList.Items[i] := value;
        end;
        Result := GetRegDataFromRegsList(run.RegsList);
      end;
    RegImport_Directory:
      begin
        Result := GetRegDataFromDirectory(IncludeTrailingPathDelimiter(run.RegSource));
      end;
    RegImport_Embedded:
      begin
        Result := GetEmbeddedRegData(run, run.RunFileName);
      end;
  end;
  if (Result = nil) then
    Result := GetEmptyRegData;
end;

function GetEmptyRegData(): PRegData;
begin
  New(Result, Create);
{$IFDEF DEBUG_OBJKIND}
  Result.fObjKind := 'TRegData';
{$ENDIF}
  Result.Files := NewKOLStrListEx;
end;

function GetRegDataFromSingleFile(fileName: KOLString): PRegData;
var
  regFile: PRegFile;
begin
  Result := GetEmptyRegData;
  regFile := ParseFile(fileName);
  regFile.CollectKeys;
  Result.Files.AddObject(fileName, PtrUInt(regFile));
end;

function GetRegDataFromRegsList(list: PKOLStrList): PRegData;
var
  i: Integer;
  fileName: KOLString;
  regFile: PRegFile;
begin
  Result := GetEmptyRegData;
  for i := 0 to list.Count - 1 do
  begin
    fileName := list.Items[i];
    regFile := ParseFile(fileName);
    regFile.CollectKeys;
    Result.Files.AddObject(fileName, PtrUInt(regFile));
  end;
end;

function GetRegDataFromDirectory(dirName: KOLString): PRegData;
var
  findHandle: THandle;
  findData: TWin32FindData;
  list: PKOLStrList;
begin
  Result := GetEmptyRegData;
  dirName := IncludeTrailingPathDelimiter(dirName);
  findHandle := FindFirstFile(PKOLChar(dirName + '*.reg'), findData);
  if findHandle <> INVALID_HANDLE_VALUE then
  begin
    list := NewKOLStrList;
    repeat
      if (findData.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
        list.Add(dirName + findData.cFileName);
    until not FindNextFile(findHandle, findData);
    FindClose(findHandle);
    Result := GetRegDataFromRegsList(list);
  end;
end;

function GetEmbeddedRegData(runData: PRunData; runFile: KOLString): PRegData;
var
  regFile: PRegFile;
begin
  Result := GetEmptyRegData;
  regFile := ParseRegSections(runData.OtherSections);
  regFile.CollectKeys;
  Result.Files.AddObject(runFile, PtrUInt(regFile));
end;

function TRegData.CloneAndGetDataFromRegistry(useGeneralKeys: Boolean): PRegData;
var
  iFile: Integer;
  regFile, newRegFile: PRegFile;
  sections: PKOLStrListEx;
begin
  Result := GetEmptyRegData;
  for iFile := 0 to Self.Files.Count - 1 do
  begin
    regFile := PRegFile(Self.Files.Objects[iFile]);
    if useGeneralKeys then
      sections := ExportGeneralKeys(regFile.GeneralKeys)
    else
      sections := ExportKeys(regFile.AllKeys);
    newRegFile := GetEmptyRegFile;
    newRegFile.Sections := sections;
    Result.Files.AddObject(Self.Files.Items[iFile], PtrUInt(newRegFile));
  end;
end;

procedure TRegData.ImportKeysToRegistry;
var
  iFile: Integer;
  regFile: PRegFile;
begin
  for iFile := 0 to Self.Files.Count - 1 do
  begin
    regFile := PRegFile(Self.Files.Objects[iFile]);
    ImportKeys(regFile.Sections);
  end;
end;

procedure TRegData.DeleteKeysFromRegistry(useGeneralKeys: Boolean);
var
  iFile: Integer;
  regFile: PRegFile;
begin
  for iFile := 0 to Self.Files.Count - 1 do
  begin
    regFile := PRegFile(Self.Files.Objects[iFile]);
    if useGeneralKeys then
      DeleteKeys(regFile.GeneralKeys)
    else
      DeleteKeys(regFile.AllKeys);
  end;
end;

//==================================================

function GetEmptyRegFile(): PRegFile;
begin
  New(Result, Create);
{$IFDEF DEBUG_OBJKIND}
  Result.fObjKind := 'TRegFile';
{$ENDIF}
  Result.Sections := NewKOLStrListEx;
end;

function ParseFile(fileName: KOLString): PRegFile;
var
  regAsIni: PXIniFile;
begin
  regAsIni := OpenXIniFile(FileName);
  regAsIni.Mode := ifmRead;
  Result := ParseRegSections(regAsIni.Sections);
end;

function ParseRegSections(sections: PKOLStrListEx): PRegFile;
var
  lineStr: KOLString;
  lines, keyContent: PKOLStrList;
  i, lineIndex: Integer;
  curSection: KOLString;
begin
  New(Result, Create);
{$IFDEF DEBUG_OBJKIND}
  Result.fObjKind := 'TRegFile';
{$ENDIF}
  Result.Sections := NewKOLStrListEx;
  for i := 1 to sections.Count - 1 do
  begin
    keyContent := NewKOLStrList;
    lines := PKOLStrList(sections.Objects[i]);
    lineIndex := 1;
    while lineIndex < lines.Count do
    begin
      lineStr := lines.Items[lineIndex];
      while lineStr[Length(lineStr)] = '\' do
      begin
        Inc(lineIndex);
        DeleteTail(lineStr, 1);
        lineStr := lineStr + TrimLeft(lines.Items[lineIndex]);
      end;
      keyContent.Add(lineStr);
      Inc(lineIndex);
    end;
    curSection := sections.Items[i];
    if (curSection <> '') and (curSection[1] <> '-') then
    begin
      Result.Sections.AddObject(curSection, PtrUInt(keyContent));
    end
    else
    begin
      keyContent.Free;
    end;
  end;
end;

procedure TRegFile.CollectKeys();
var
  i: Integer;
begin
  AllKeys := NewKOLStrList;
  GeneralKeys := NewKOLStrList;
  for i := 0 to Sections.Count - 1 do
    MergeKeyList(AllKeys, GeneralKeys, Sections.Items[i]);
end;

end.

