unit FileTools;

interface

uses Windows, KOL;

procedure CopyFileSourceTarget(source, target: KOLString);
procedure RemoveFileSource(source: KOLString);

function ExtractLastElementOfPath(Path: KOLString): KOLString;
function FullDirectoryCopy(SourceDir, TargetDir: KOLString; StopIfNotAllCopied, OverWriteFiles: Boolean): Boolean;
function FullRemoveDir(Dir: KOLstring; DeleteAllFilesAndFolders, StopIfNotAllDeleted, RemoveRoot: boolean): Boolean;
function DeleteAllFiles(const NameMask: KOLString): Boolean;
function CopyAllFiles(const NameMask: KOLString; const TargetPath: KOLString; FailIfExists: Boolean): Boolean;

implementation

procedure CopyFileSourceTarget(source, target: KOLString);
begin
  ForceDirectories(ExtractFilePath(target));
  if (source[Length(source)] = '\') then
  begin //����������� �����
    if (target[Length(target)] = '\') then
      target := target + ExtractLastElementOfPath(source)
    else
      target := IncludeTrailingPathDelimiter(target);
    FullDirectoryCopy(source, target, False, True);
  end
  else //����������� �����
  begin
    if (target[Length(target)] = '\') then
    begin
      CopyAllFiles(source, target, False);
    end
    else
    begin
      CopyFile(PKOLChar(source), PKOLChar(target), False);
    end;
  end;
end;

procedure RemoveFileSource(source: KOLString);
begin
  if (source[Length(source)] = '\') then
  begin //�������� �����
    FullRemoveDir(source, True, False, True);
  end
  else
  begin //�������� �����
    DeleteAllFiles(source);
  end;
end;

function ExtractLastElementOfPath(Path: KOLString): KOLString;
var
  I: Integer;
begin
  I := Length(Path);
  if Path[I] = '\' then
    Dec(I);
  while (I > 0) and (Path[I] <> '\') do
    Dec(I);
  Result := Copy(Path, I + 1, MaxInt);
end;

function FullDirectoryCopy(SourceDir, TargetDir: KOLString; StopIfNotAllCopied, OverWriteFiles: Boolean): Boolean;
var
  FindHandle: THandle;
  FindData: TWin32FindData;
begin
  Result := False;
  if not DirectoryExists(SourceDir) then Exit;
  if not ForceDirectories(TargetDir) then Exit;
  FindHandle := FindFirstFile(PWideChar(SourceDir + '*'), FindData);
  FindNextFile(FindHandle, FindData);
  FindNextFile(FindHandle, FindData);
  repeat
    if (FindData.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) > 0 then
      Result := FullDirectoryCopy(SourceDir + FindData.cFileName + '\', TargetDir + FindData.cFileName + '\', StopIfNotAllCopied, OverWriteFiles)
    else
    begin
      if not (not OverWriteFiles and FileExists(TargetDir + FindData.cFileName)) then
      begin
        Result := CopyFile(PWidechar(SourceDir + FindData.cFileName), PWidechar(TargetDir + FindData.cFileName), not OverWriteFiles);
      end
      else
        Result := True;
    end;
    if not Result and StopIfNotAllCopied then
      Break;
  until not FindNextFile(FindHandle, FindData);
  FindClose(FindHandle);
end;

function FullRemoveDir(Dir: KOLstring; DeleteAllFilesAndFolders, StopIfNotAllDeleted, RemoveRoot: boolean): Boolean;
var
  FindHandle: THandle;
  FindData: TWin32FindData;
label
  fin;
begin
  if not DirectoryExists(Dir) then
  begin
    Result := False;
    Exit;
  end;
  //Result := True;

  FindHandle := FindFirstFile(PWideChar(Dir + '*'), FindData);
  FindNextFile(FindHandle, FindData);
  FindNextFile(FindHandle, FindData);

  repeat
    if (FindData.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) > 0 then
    begin
      if DeleteAllFilesAndFolders then
        SetFileAttributes(PWideChar(Dir + FindData.cFileName), FILE_ATTRIBUTE_ARCHIVE);
      Result := FullRemoveDir(PWideChar(Dir + FindData.cFileName + '\'), DeleteAllFilesAndFolders, StopIfNotAllDeleted, True);
      if not Result and StopIfNotAllDeleted then goto fin;
    end
    else
    begin
      if DeleteAllFilesAndFolders then
        SetFileAttributes(PWideChar(Dir + FindData.cFileName), FILE_ATTRIBUTE_ARCHIVE);
      Result := DeleteFile(PWideChar(Dir + FindData.cFileName));
      if not Result and StopIfNotAllDeleted then goto fin;
    end;
  until not FindNextFile(FindHandle, FindData);

  if not Result then goto fin;
  if RemoveRoot then
    if not RemoveDirectory(PWideChar(Dir)) then
      Result := false;
  fin:
  FindClose(FindHandle);
end;

function DeleteAllFiles(const NameMask: KOLString): Boolean;
var
  Files, Name: KOLString;
begin
  Files := GetFileListStr(ExtractFilePath(NameMask), ExtractFileName(NameMask));
  Result := TRUE;
  while Files <> '' do
  begin
    Name := Parse(Files, FileOpSeparator);
    Result := DeleteFile(PKOLChar(Name)) and Result;
  end;
end;

function CopyAllFiles(const NameMask: KOLString; const TargetPath: KOLString; FailIfExists: Boolean): Boolean;
var
  Files, Name: KOLString;
begin
  Files := GetFileListStr(ExtractFilePath(NameMask), ExtractFileName(NameMask));
  Result := TRUE;
  while Files <> '' do
  begin
    Name := Parse(Files, FileOpSeparator);
    Result := CopyFile(PKOLChar(Name), PKOLChar(TargetPath + ExtractFileName(Name)), FailIfExists) and Result;
  end;
end;

end.

