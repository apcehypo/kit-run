{$DEFINE UNICODE_CTRLS}
{$I KOLDEF.inc}
unit XIniFile; //light. full version is in kIT Universal Presets

{========================================
XIniFile by ApceH Hypocrite � 2012
========================================}

interface

uses
  Windows, KOL;

const
  CommentsStub: WideChar = #$FFFD;

type
  TEncodingType = (ANSI, UTF8, UTF16LE, UTF16BE, UTF32LE, UTF32BE);
  TKeyStatus = (EXISTS, IS_EMPTY, NOT_EXISTS);

  PXIniFile = ^TXIniFile;
  TXIniFile = object(TObj)
    //===== ����
  protected fFileName: KOLString;
  protected fSections: PKOLStrListEx;
  protected fEncoding: TEncodingType;
  protected fText: KOLString;
  protected fCurrentSection: Integer;
  protected fCurrentSectionName: KOLString;
  protected fMode: TIniFileMode;
  protected fCaseSensitive: Boolean;
    //  protected fCommentsAllowed: Boolean;
  protected fAutoSave: Boolean;
  protected fChanged: Boolean;
  protected fFileExisted: Boolean;
  protected function GetSectionsCount(): Integer;
    //===== ��������
  public property FileName: KOLString read fFileName;
  public property Sections: PKOLStrListEx read fSections write fSections;
  public property Encoding: TEncodingType read fEncoding;
  public property Text: KOLString read fText;
  public property SectionsCount: Integer read GetSectionsCount;
  public property CaseSensitive: Boolean read fCaseSensitive write fCaseSensitive;
    //  public property CommentsAllowed: Boolean read fCommentsAllowed write fCommentsAllowed;
  public property AutoSave: Boolean read fAutoSave write fAutoSave;
  public property FileExisted: Boolean read fFileExisted;

    //===== ������
  public destructor Destroy; virtual;

    //public procedure ClearAll;
    //public procedure ClearSection(iSection: Integer); overload;
    //public procedure ClearSection(sect: KOLString); overload;

    //public procedure DeleteAll;
  public procedure DeleteSection(iSection: Integer); overload;
  public procedure DeleteSection(sect: KOLString); overload;
    //public procedure DeleteKey(iSection: Integer; index: Integer); overload;
    //public procedure DeleteKey(iSection: Integer; keyName: KOLString); overload;

    //public procedure RenameSection(iSection: Integer; newName: KOLString);
    //public procedure RenameKey(iSection: Integer; oldName, newName: KOLString);

  public function GetSectionNames: PKOLStrList;
  public function SectionIndex(sect: KOLString): Integer;
  public function SectionHeader(iSection: Integer): KOLString;
  public function KeyIndex(iSection: Integer; keyName: KOLString): Integer; overload;
  public function KeyIndex(keyName: KOLString): Integer; overload;

  public function GetSectionData(index: Integer): PKOLStrList; overload;
  public function GetSectionData(sect: KOLString): PKOLStrList; overload;
  public function GetSectionData(): PKOLStrList; overload;

  public procedure SetValueBoolean(iSection: Integer; keyName: KOLString; value: Boolean); overload;
  public procedure SetValueBoolean(sect: KOLString; keyName: KOLString; value: Boolean); overload;
  public procedure SetValueDouble(iSection: Integer; keyName: KOLString; value: Double); overload;
  public procedure SetValueDouble(sect: KOLString; keyName: KOLString; value: Double); overload;
  public procedure SetValueInteger(iSection: Integer; keyName: KOLString; value: Integer); overload;
  public procedure SetValueInteger(sect: KOLString; keyName: KOLString; value: Integer); overload;
  public procedure SetValueString(iSection: Integer; keyName: KOLString; value: KOLString); overload;
  public procedure SetValueString(sect: KOLString; keyName: KOLString; value: KOLString); overload;
  public procedure SetValueString(keyName: KOLString; value: KOLString); overload;
    //public procedure SetEmptyValue(iSection: Integer; keyName: KOLString); overload;
    //public procedure SetEmptyValue(sect: KOLString; keyName: KOLString); overload;

  public function GetValueBoolean(iSection: Integer; keyName: KOLString; out value: Boolean): TKeyStatus; overload;
  public function GetValueBoolean(sect: KOLString; keyName: KOLString; out value: Boolean): TKeyStatus; overload;
  public function GetValueDouble(iSection: Integer; keyName: KOLString; out value: Double): TKeyStatus; overload;
  public function GetValueDouble(sect: KOLString; keyName: KOLString; out value: Double): TKeyStatus; overload;
  public function GetValueInteger(iSection: Integer; keyName: KOLString; out value: Integer): TKeyStatus; overload;
  public function GetValueInteger(sect: KOLString; keyName: KOLString; out value: Integer): TKeyStatus; overload;
  public function GetValueString(iSection: Integer; keyName: KOLString; out value: KOLString): TKeyStatus; overload;
  public function GetValueString(sect: KOLString; keyName: KOLString; out value: KOLString): TKeyStatus; overload;
  public function GetValueString(keyName: KOLString; out value: KOLString): TKeyStatus; overload;

  public function KeyValue(iSection: Integer; iName: Integer; out keyName: KOLString; out value: KOLString): TKeyStatus; overload;
  public function KeysCount(iSection: Integer): Integer; overload;
  public function KeysCount: Integer; overload;

  public procedure AddSection(sect: KOLString);
  public procedure AddKey(iSection: Integer; keyName, value: KOLString); overload;
  public procedure AddKey(keyName, value: KOLString); overload;

    //public procedure InsertSection(iPrevSect: Integer; sect: KOLString; data: PKOLStrList);

  public function Save: Boolean; overload;
  public function Save(newFileName: KOLString; enc: TEncodingType): Boolean; overload;
  public function ForceSave: Boolean;
    //=====
    // TIniFile-���������
  public procedure SetCurrentSectionIndex(index: Integer);
  public procedure SetCurrentSection(sect: KOLString);
  public function GetCurrentSection: KOLString;
  public property Section: KOLString read GetCurrentSection write SetCurrentSection;
  public property Mode: TIniFileMode read fMode write fMode;
  public function ValueString(const key, value: KOLString): KOLString;
  public function ValueBoolean(const key: KOLString; value: Boolean): Boolean;
  public function ValueInteger(const key: KOLString; value: Integer): Integer;
  public function ValueDouble(const key: KOLString; value: Double): Double;
    //public procedure SectionData(var data: PKOLStrList);
    //public procedure ClearSection; overload;
  public procedure DeleteSection; overload;
    //public procedure DeleteKey(keyName: KOLString); overload;
      //=====
  end;

function OpenXIniFile(fileName: KOLString): PXIniFile; Overload;
//function OpenXIniFile(fileName: KOLString; CaseSens, {Comments,} AutoSav: Boolean): PXIniFile; Overload;
function CreateXIniFromText(Text: KOLString): PXIniFile;
function CreateXIniFromCharArray(buf: PAnsiChar; bufLength: Cardinal): PXIniFile;
function CreateEmptyXIniFile: PXIniFile;
function DetectEncodingType(hOpenedFile: THandle): TEncodingType;

implementation

(*function OpenXIniFile(fileName: KOLString; CaseSens, AutoSav {, Comments, Spaces}: Boolean): PXIniFile;
begin
  Result := OpenXIniFile(fileName);
  Result.CaseSensitive := CaseSens;
  Result.AutoSave := AutoSav;
  //  Result.CommentsAllowed := Comments;
  //  Result.KeepWhiteSpaces := Spaces;
end;(**)

function ParseXIniText(Text: KOLString): PXIniFile;
var
  line: KOLString;
  sect: KOLString;
  curData: PKOLStrList;
begin
  New(Result, Create);
{$IFDEF DEBUG_OBJKIND}
  Result.fObjKind := 'TXIniFile';
{$ENDIF}
  Result.fSections := NewWStrListEx;
  Result.fSections.Add('');
  curData := NewWStrList;
  curData.Add(''); // � ���������� ������ �� ����� ������������ ���������
  Result.fSections.Objects[0] := PtrUInt(curData);
  Result.CaseSensitive := True;
  Result.fChanged := False;
  Result.fText := Text;
  while Result.fText <> '' do
  begin
    line := Parse(Result.fText, #13);
    ParseW(Result.fText, #10);
    line := TrimLeft(line);
    if line <> '' then
    begin
      if (line[1] = ';') and (Length(line) > 1) and (line[2] = '[') then
      begin //������������������ ��������� ������
        //Parse(line, '[');
        //sect := CommentsStub + ParseW(line, ']');
        //Result.fSections.Add(sect);
        curData := nil;
        //Result.fSections.Objects[Result.SectionsCount] := PtrUInt(curData);
        //curData.Add(line);
      end
      else
        if line[1] = '[' then
        begin //���������� ���������
          ParseW(line, '[');
          sect := ParseW(line, ']');
          Result.fSections.Add(sect);
          curData := NewWStrList;
          Result.fSections.Objects[Result.SectionsCount] := PtrUInt(curData);
          curData.Add(line); //���� �� ���������� ��������� ���-��, ��� ����� �� �������� �������
        end
        else //������ ����� �� ������
        begin
          if (line[1] = ';') then Continue; //�������� ������������
          if (curData <> nil) then //�� ������������������ ������
            curData.Add(line);
        end;
    end;
  end;

end;

function OpenXIniFile(fileName: KOLString): PXIniFile;
//�������� Ini-�����
var
  hFile: THandle;
  fLen: Cardinal;
  buf: PAnsiChar;
  content: KOLString;
  fFileExisted: Boolean;
  fEncoding: TEncodingType;
begin
  fFileExisted := False;
  fEncoding := ANSI;
  if FileExists(fileName) then
  begin
    fFileExisted := True;
    //��������� ������ ������� �� ������ �� ���������
    hFile := FileCreate(fileName, ofOpenRead or ofOpenExisting); // or ofShareDenyRead);
    if hFile = INVALID_HANDLE_VALUE then
    begin
      Result := nil;
      Exit;
    end;
    fEncoding := DetectEncodingType(hFile);
    fLen := FileSize(fileName);
    GetMem(buf, fLen + 1);
    ZeroMemory(buf, fLen + 1);
    FileRead(hFile, buf[0], fLen);
    FileClose(hFile);
    case fEncoding of
      ANSI: content := KOLString(buf);
      UTF8:
        begin
          buf[fLen - 3] := #$00;
          content := UTF8Decode(buf);
        end
    else
      content := PWideChar(buf);
    end;
    FreeMem(buf);
    //���������� ������ � ������ �����
  end;
  Result := ParseXIniText(content);
  Result.fFileName := fileName;
  Result.AutoSave := True;
  Result.fFileExisted := fFileExisted;
  Result.fEncoding := fEncoding;
end; (**)

function CreateEmptyXIniFile: PXIniFile;
var
  curData: PKOLStrList;
begin
  New(Result, Create);
{$IFDEF DEBUG_OBJKIND}
  Result.fObjKind := 'TXIniFile';
{$ENDIF}
  Result.fSections := NewWStrListEx;
  Result.fSections.Add('');
  curData := NewKOLStrList;
  curData.Add(''); // � ���������� ������ �� ����� ������������ ���������
  Result.fSections.Objects[0] := PtrUInt(curData);
  Result.fMode := ifmRead;
  Result.fCurrentSection := 0;
end;

function CreateXIniFromText(Text: KOLString): PXIniFile;
begin
  Result := ParseXIniText(Text);
  Result.fFileName := '';
  Result.AutoSave := False;
  Result.fFileExisted := False;
end;

function CreateXIniFromCharArray(buf: PAnsiChar; bufLength: Cardinal): PXIniFile;
var
  Encoding: TEncodingType;
  Text: KOLString;
begin
  Encoding := ANSI;
  case buf[0] of
    #$EF:
      begin
        if (buf[1] = #$BB) and (buf[2] = #$BF) then
        begin
          Encoding := UTF8;
        end;
      end;
    #$FE:
      begin
        if (buf[1] = #$FF) then
        begin
          Encoding := UTF16BE;
        end;
      end;
    #$FF:
      begin
        if (buf[1] = #$FE) then
          if (buf[2] = #$00) and (buf[3] = #$00) then
          begin
            Encoding := UTF32LE;
          end
          else
          begin
            Encoding := UTF16LE;
          end;
      end;
    #$00:
      begin
        if (buf[1] = #$00) and (buf[2] = #$FE) and (buf[3] = #$FF) then
        begin
          Encoding := UTF32BE;
        end;
      end;
  end;
  case Encoding of
    ANSI: Text := KOLString(buf);
    UTF8:
      begin
        buf[bufLength - 3] := #$00;
        Text := UTF8Decode(buf);
      end
  else
    Text := PWideChar(buf);
  end;
  Result := CreateXIniFromText(Text);
end;

function DetectEncodingType(hOpenedFile: THandle): TEncodingType;
//����� ����������� ���������
var
  buf: array[0..3] of AnsiChar;
begin
  Result := ANSI;
  FileRead(hOpenedFile, buf, 4);
  case buf[0] of
    #$EF:
      begin
        if (buf[1] = #$BB) and (buf[2] = #$BF) then
        begin
          FileSeek(hOpenedFile, 3, spBegin);
          Result := UTF8;
        end;
      end;
    #$FE:
      begin
        if (buf[1] = #$FF) then
        begin
          FileSeek(hOpenedFile, 2, spBegin);
          Result := UTF16BE;
        end;
      end;
    #$FF:
      begin
        if (buf[1] = #$FE) then
          if (buf[2] = #$00) and (buf[3] = #$00) then
          begin
            FileSeek(hOpenedFile, 4, spBegin);
            Result := UTF32LE;
          end
          else
          begin
            FileSeek(hOpenedFile, 2, spBegin);
            Result := UTF16LE;
          end;
      end;
    #$00:
      begin
        if (buf[1] = #$00) and (buf[2] = #$FE) and (buf[3] = #$FF) then
        begin
          FileSeek(hOpenedFile, 4, spBegin);
          Result := UTF32BE;
        end;
      end;
  else
    FileSeek(hOpenedFile, 0, spBegin);
  end;
end; (**)

function IndexOfName(data: PKOLStrList; keyName: KOLString; cs: Boolean): Integer;
var
  i: Integer;
  found: Boolean;
  value, namestr: KOLString;
begin
  if not cs then
    keyName := WLowerCase(keyName);
  found := False;
  for i := 1 to data.Count - 1 do
  begin
    value := data.Items[i];
    namestr := ParseW(value, '=');
    if not cs then
      namestr := WLowerCase(namestr);
    if namestr = keyName then
    begin
      found := true;
      Break;
    end;
  end;
  if found then
    Result := i
  else
    Result := -1;
end;

function UnicodeToAnsiString(const ws: KOLString; codePage: Word): AnsiString;
  overload;
var
  l: integer;
begin
  if ws = '' then
    Result := ''
  else
  begin
    l := WideCharToMultiByte(codePage, WC_COMPOSITECHECK or WC_DISCARDNS or WC_SEPCHARS or WC_DEFAULTCHAR, @ws[1], -1, nil, 0, nil, nil);
    SetLength(Result, l - 1);
    if l > 1 then
      WideCharToMultiByte(codePage, WC_COMPOSITECHECK or WC_DISCARDNS or WC_SEPCHARS or WC_DEFAULTCHAR, PWideChar(ws), -1, @Result[1], l - 1, nil, nil);
  end;
end; (**)

{>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>}

(*procedure TXIniFile.ClearSection;
begin
  ClearSection(fCurrentSection);
end;(**)

procedure TXIniFile.DeleteSection;
begin
  DeleteSection(fCurrentSection);
end;

procedure TXIniFile.DeleteSection(sect: KOLString);
begin
  DeleteSection(SectionIndex(sect));
end;

(*procedure TXIniFile.DeleteKey(KeyName: KOLString);
begin
  DeleteKey(fCurrentSection, KeyName);
end;(**)

function TXIniFile.ValueString(const key, value: KOLString): KOLString;
var
  valout: KOLString;
begin
  Result := value;
  if fMode = ifmRead then
  begin //value - �������� �� ���������
    if GetValueString(fCurrentSection, key, valout) = EXISTS then
      Result := valout
  end
  else
    SetValueString(fCurrentSection, Key, value);
end;

function TXIniFile.ValueBoolean(const key: KOLString; value: Boolean): Boolean;
var
  valout: Boolean;
begin
  Result := value;
  if fMode = ifmRead then
  begin //value - �������� �� ���������
    if GetValueBoolean(fCurrentSection, key, valout) = EXISTS then
      Result := valout
  end
  else
    SetValueBoolean(fCurrentSection, Key, value);
end;

function TXIniFile.ValueInteger(const key: KOLString; value: Integer): Integer;
var
  valout: Integer;
begin
  Result := value;
  if fMode = ifmRead then
  begin //value - �������� �� ���������
    if GetValueInteger(fCurrentSection, key, valout) = EXISTS then
      Result := valout
  end
  else
    SetValueInteger(fCurrentSection, Key, value);
end;

function TXIniFile.ValueDouble(const key: KOLString; value: Double): Double;
var
  valout: Double;
begin
  Result := value;
  if fMode = ifmRead then
  begin //value - �������� �� ���������
    if GetValueDouble(fCurrentSection, key, valout) = EXISTS then
      Result := valout
  end
  else
    SetValueDouble(fCurrentSection, Key, value);
end;

(*procedure TXIniFile.SectionData(var data: PKOLStrList);
var
  sectData, temp: PKOLStrList;
begin
  if fMode = ifmRead then
  begin
    temp := PKOLStrList(fSections.Objects[fCurrentSection]);
    if temp <> nil then
    begin
      sectData := NewWStrList;
      sectData.AddWStrings(temp);
      sectData.Delete(0);
      data.AddWStrings(sectData);
    end
  end
  else
  begin
    if fCurrentSection >= 0 then
      PKOLStrList(fSections.Objects[fCurrentSection]).Free
    else //����� ������
      fCurrentSection := fSections.Add(fCurrentSectionName);
    sectData := NewWStrList;
    sectData.Add('');
    sectData.AddWStrings(data);
    fSections.Objects[fCurrentSection] := PtrUInt(sectData);
    fChanged := True;
    if AutoSave then
      Save;
  end;
end;(**)

procedure TXIniFile.SetCurrentSectionIndex(index: Integer);
begin
  if (index >= 0) and (index < fSections.Count) then
    fCurrentSection := index
  else
    fCurrentSection := 0;
end;

procedure TXIniFile.SetCurrentSection(sect: KOLString);
begin
  fCurrentSectionName := sect;
  fCurrentSection := SectionIndex(sect);
end;

function TXIniFile.GetCurrentSection: KOLString;
begin
  Result := fSections.Items[fCurrentSection];
end;

function TXIniFile.Save(newFileName: KOLString; enc: TEncodingType): Boolean;
const
  UTF16LEHeader = #$FF#$FE;
  UTF16BEHeader = #$FE#$FF;
  UTF8Header = #$EF#$BB#$BF;
var
  temp: AnsiChar;
  i, j: Integer;
  content: KOLString;
  curData: PKOLStrList;
  hFile: THandle;
  pcontent: PAnsiChar;
  ansicontent: AnsiString;
  len: Cardinal;
begin
  Result := False;
  content := '';
  curData := PKOLStrList(fSections.Objects[0]);
  //������� (������) ������
  for j := 1 to curData.Count - 1 do
  begin
    content := content + curData.Items[j] + sLineBreak;
  end;
  if curData.Count > 1 then
    content := content + sLineBreak;
  //������� ������
  for i := 1 to SectionsCount do
  begin
    if (Length(fSections.Items[i]) > 0) and (fSections.Items[i][1] = CommentsStub) then
      //������������������ ���������
      content := content + ';[' + Copy(fSections.Items[i], 2, MaxInt) + ']'
    else
      //���������� ���������
      content := content + '[' + fSections.Items[i] + ']';
    curData := PKOLStrList(fSections.Objects[i]);
    for j := 0 to curData.Count - 1 do
    begin
      content := content + curData.Items[j] + sLineBreak;
    end;
    content := content + sLineBreak;
  end;
  if FileExists(fileName) then
    hFile := FileCreate(fileName, ofOpenWrite or ofTruncateExisting or ofShareDenyWrite)
  else
    hFile := FileCreate(fileName, ofOpenWrite or ofOpenAlways or ofShareDenyWrite);
  if hFile <> INVALID_HANDLE_VALUE then
  begin
    //���������� � ������ ���������
    case enc of
      ANSI:
        begin
          ansicontent := UnicodeToAnsiString(content, GetACP);
          len := Length(ansicontent);
          pcontent := @ansicontent[1];
        end;
      UTF8:
        begin
          _lwrite(hFile, UTF8Header, 3);
          len := UnicodeToUtf8(nil, PWideChar(content), MaxInt) - 1;
          pcontent := PAnsiChar(UTF8Encode(content));
        end;
      UTF16BE:
        begin
          _lwrite(hFile, UTF16BEHeader, 2);
          len := Length(content);
{$WARNINGS OFF}
          pcontent := PAnsiChar(content);
{$WARNINGS ON}
          while len > 0 do
          begin
            temp := pcontent[0];
            pcontent[0] := pcontent[1];
            pcontent[1] := temp;
            Dec(len);
            Inc(pcontent, 2);
          end;
{$WARNINGS OFF}
          pcontent := PAnsiChar(content);
{$WARNINGS ON}
          len := Length(content) * 2;
        end;
      //UTF16LE:
    else
      begin
        _lwrite(hFile, UTF16LEHeader, 2);
        len := Length(content) * 2;
{$WARNINGS OFF}
        pcontent := PAnsiChar(content);
{$WARNINGS ON}
      end;
    end;
    _lwrite(hFile, pcontent, len);
    FileClose(hFile);
  end;
  content := '';
end;

function TXIniFile.ForceSave: Boolean;
begin
  Result := Save(fFileName, fEncoding);
  fChanged := False;
end;

function TXIniFile.Save: Boolean;
begin
  if fChanged then
    Result := Save(fFileName, fEncoding)
  else
    Result := True;
  fChanged := False;
end;

destructor TXIniFile.Destroy;
var
  i: Integer;
begin
  if AutoSave then
    Save;
  for i := 0 to SectionsCount do
    PKOLStrList(fSections.Objects[i]).Free;
  fSections.Free;
  inherited;
end;

function TXIniFile.KeysCount(iSection: Integer): Integer;
var
  curData: PKOLStrList;
begin
  curData := GetSectionData(iSection);
  if curData <> nil then
    Result := curData.Count
  else
    Result := 0;
end;

function TXIniFile.KeysCount: Integer;
begin
  Result := KeysCount(fCurrentSection);
end;

procedure TXIniFile.SetValueString(iSection: Integer; keyName: KOLString; value: KOLString);
var
  curData: PKOLStrList;
  iName: Integer;
  line: KOLString;
begin
  if iSection < 0 then
    if fCurrentSectionName = '' then
      Exit
    else
    begin
      curData := NewWStrList;
      curData.Add('');
      iSection := fSections.Add(fCurrentSectionName);
      fSections.Objects[iSection] := PtrUInt(curData);
      fCurrentSection := iSection;
    end
  else
    curData := GetSectionData(iSection);
  if curData <> nil then
  begin
    line := keyName + '=' + value;
    iName := IndexOfName(curData, keyName, CaseSensitive);
    if iName >= 0 then
    begin
      curData.Items[iName] := line;
    end
    else
    begin
      //�������� ���� ���� ��� ������� '='
      if CaseSensitive then
        iName := curData.IndexOf(keyName)
      else
        iName := curData.IndexOf_NoCase(keyName);
      if iName >= 0 then
        curData.Items[iName] := line
      else
        curData.Add(line);
    end;
  end;
  fChanged := True;
  if AutoSave then
    Save;
end;

procedure TXIniFile.SetValueBoolean(iSection: Integer; keyName: KOLString; value: Boolean);
begin
  SetValueString(iSection, keyName, Int2Str(Integer(value)));
end;

procedure TXIniFile.SetValueBoolean(sect: KOLString; keyName: KOLString; value: Boolean);
begin
  SetValueString(sect, keyName, Int2Str(Integer(value)));
end;

procedure TXIniFile.SetValueInteger(iSection: Integer; keyName: KOLString; value: Integer);
begin
  SetValueString(iSection, keyName, Int2Str(value));
end;

procedure TXIniFile.SetValueInteger(sect: KOLString; keyName: KOLString; value: Integer);
begin
  SetValueString(sect, keyName, Int2Str(value));
end;

procedure TXIniFile.SetValueDouble(iSection: Integer; keyName: KOLString; value: Double);
begin
  SetValueString(iSection, keyName, Extended2Str(value));
end;

procedure TXIniFile.SetValueDouble(sect: KOLString; keyName: KOLString; value: Double);
begin
  SetValueString(sect, keyName, Extended2Str(value));
end;

procedure TXIniFile.SetValueString(sect: KOLString; keyName: KOLString; value: KOLString);
var
  iSection: Integer;
  newSection: PKOLStrList;
begin
  iSection := SectionIndex(sect);
  if iSection < 0 then
  begin //����� ������
    iSection := fSections.Add(sect);
    newSection := NewWStrList;
    newSection.Add(''); //������� ������ ��������������
    fSections.Objects[iSection] := PtrUInt(newSection);
  end;
  SetValueString(iSection, keyName, value);
end;

procedure TXIniFile.SetValueString(keyName: KOLString; value: KOLString);
begin
  SetValueString(fCurrentSection, keyName, value);
end;

(*procedure TXIniFile.SetEmptyValue(iSection: Integer; keyName: KOLString);
var
  curData: PKOLStrList;
  iName: Integer;
begin
  curData := GetSectionData(iSection);
  if curData <> nil then
  begin
    iName := IndexOfName(curData, keyName, CaseSensitive);
    if iName >= 0 then
    begin
      curData.Items[iName] := keyName;
    end
    else
    begin
      //�������� ���� ���� ��� ������� '='
      if CaseSensitive then
        iName := curData.IndexOf(keyName)
      else
        iName := curData.IndexOf_NoCase(keyName);
      if iName >= 0 then
        curData.Items[iName] := keyName
      else
        curData.Add(keyName);
    end;
  end;
  fChanged := True;
  if AutoSave then
    Save;
end;(**)

(*procedure TXIniFile.SetEmptyValue(sect: KOLString; keyName: KOLString);
var
  iSection: Integer;
  newSection: PKOLStrList;
begin
  iSection := SectionIndex(sect);
  if iSection < 0 then
  begin //����� ������
    iSection := fSections.Add(sect);
    newSection := NewWStrList;
    newSection.Add(''); //������� ������ ��������������
    fSections.Objects[iSection] := PtrUInt(newSection);
  end;
  SetEmptyValue(iSection, keyName);
end;(**)

function TXIniFile.GetValueString(iSection: Integer; keyName: KOLString; out value: KOLString): TKeyStatus;
var
  curData: PKOLStrList;
  iName: Integer;
begin
  Result := NOT_EXISTS;
  value := '';
  curData := GetSectionData(iSection);
  if curData <> nil then
  begin
    iName := IndexOfName(curData, keyName, CaseSensitive);
    if iName >= 0 then
    begin
      Result := EXISTS;
      value := curData.Items[iName];
      ParseW(value, '=');
    end
    else
    begin
      //�������� ���� ���� ��� ������� '='
      if CaseSensitive then
        iName := curData.IndexOf(keyName)
      else
        iName := curData.IndexOf_NoCase(keyName);
      if iName >= 0 then
        Result := IS_EMPTY
      else
        Result := NOT_EXISTS;
    end;
  end;
end;

function TXIniFile.GetValueString(sect: KOLString; keyName: KOLString; out value: KOLString): TKeyStatus;
begin
  Result := GetValueString(SectionIndex(sect), keyName, value);
end;

function TXIniFile.GetValueString(keyName: KOLString; out value: KOLString): TKeyStatus;
begin
  Result := GetValueString(fCurrentSection, keyName, value);
end;

function TXIniFile.GetValueBoolean(iSection: Integer; keyName: KOLString; out value: Boolean): TKeyStatus;
var
  strval: KOLString;
begin
  Result := GetValueString(iSection, keyName, strval);
  value := strval = '1';
end;

function TXIniFile.GetValueBoolean(sect: KOLString; keyName: KOLString; out value: Boolean): TKeyStatus;
begin
  Result := GetValueBoolean(SectionIndex(sect), keyName, value);
end;

function TXIniFile.GetValueDouble(iSection: Integer; keyName: KOLString; out value: Double): TKeyStatus;
var
  strval: KOLString;
begin
  Result := GetValueString(iSection, keyName, strval);
  value := Str2Double(strval);
end;

function TXIniFile.GetValueDouble(sect: KOLString; keyName: KOLString; out value: Double): TKeyStatus;
begin
  Result := GetValueDouble(SectionIndex(sect), keyName, value);
end;

function TXIniFile.GetValueInteger(iSection: Integer; keyName: KOLString; out value: Integer): TKeyStatus;
var
  strval: KOLString;
begin
  Result := GetValueString(iSection, keyName, strval);
  value := Str2Int(strval);
end;

function TXIniFile.GetValueInteger(sect: KOLString; keyName: KOLString; out value: Integer): TKeyStatus;
begin
  Result := GetValueInteger(SectionIndex(sect), keyName, value);
end;

function TXIniFile.KeyValue(iSection: Integer; iName: Integer; out keyName: KOLString; out value: KOLString): TKeyStatus;
var
  curData: PKOLStrList;
begin
  Result := NOT_EXISTS;
  keyName := '';
  value := '';
  curData := GetSectionData(iSection);
  if curData <> nil then
  begin
    if (iName >= 0) and (iName < curData.Count) then
    begin
      value := curData.Items[iName];
      if IndexOfChar(value, '=') > 0 then
      begin
        keyName := ParseW(value, '=');
        Result := EXISTS;
      end
      else
      begin
        keyName := value;
        value := '';
        Result := IS_EMPTY;
      end;
    end;
  end;
end;

function TXIniFile.GetSectionNames: PKOLStrList;
var
  i: Integer;
begin
  Result := NewWStrList;
  for i := 1 to fSections.Count - 1 do
    Result.Add(fSections.Items[i]);
end;

function TXIniFile.GetSectionsCount(): Integer;
begin
  Result := fSections.Count - 1;
end;

function TXIniFile.SectionIndex(sect: KOLString): Integer;
begin
  if CaseSensitive then
    Result := fSections.IndexOf(sect)
  else
    Result := fSections.IndexOf_NoCase(sect);
end;

function TXIniFile.SectionHeader(iSection: Integer): KOLString;
begin
  if (iSection >= 0) and (iSection <= SectionsCount) then
    Result := fSections.Items[iSection];
end;

function TXIniFile.KeyIndex(iSection: Integer; keyName: KOLString): Integer;
begin
  if (iSection >= 0) and (iSection <= SectionsCount) then
  begin
    Result := IndexOfName(PKOLStrList(fSections.Objects[iSection]), keyName, CaseSensitive);
  end
  else
    Result := -2;
end;

function TXIniFile.KeyIndex(keyName: KOLString): Integer;
begin
  Result := KeyIndex(fCurrentSection, keyName);
end;

function TXIniFile.GetSectionData(index: Integer): PKOLStrList;
begin
  Result := nil;
  if (index >= 0) and (index < fSections.Count) then
    Result := PKOLStrList(fSections.Objects[index]);
end;

function TXIniFile.GetSectionData(sect: KOLString): PKOLStrList;
begin
  Result := GetSectionData(SectionIndex(sect));
end;

function TXIniFile.GetSectionData(): PKOLStrList;
begin
  Result := GetSectionData(fCurrentSection);
end;

(*procedure TXIniFile.DeleteKey(iSection: Integer; index: Integer);
var
  curData: PKOLStrList;
begin
  if (iSection > 0) and (iSection <= SectionsCount) then
  begin
    curData := PKOLStrList(fSections.Objects[iSection]);
    if (index >= 0) and (index < curData.Count) then
    begin
      if index = 0 then
        curData.Items[0] := ''
      else
        curData.Delete(index);
      fChanged := True;
      if AutoSave then
        Save;
    end;
  end;
end;(**)

(*procedure TXIniFile.DeleteKey(iSection: Integer; keyName: KOLString);
var
  iName: Integer;
  curData: PKOLStrList;
begin
  if (iSection > 0) and (iSection < fSections.Count) then
  begin
    curData := PKOLStrList(fSections.Objects[iSection]);
    iName := IndexOfName(curData, keyName, CaseSensitive);
    if iName >= 0 then
      curData.Delete(iName)
    else
    begin
      //�������� ���� ���� ��� ������� '='
      if CaseSensitive then
        iName := curData.IndexOf(keyName)
      else
        iName := curData.IndexOf_NoCase(keyName);
      if iName >= 0 then
        curData.Delete(iName);
    end;
    fChanged := True;
    if AutoSave then
      Save;
  end;
end;(**)

(*procedure TXIniFile.ClearSection(iSection: Integer);
var
  curData: PKOLStrList;
begin
  if (iSection > 0) and (iSection < fSections.Count) then
  begin
    curData := PKOLStrList(fSections.Objects[iSection]);
    curData.Clear;
    curData.Add('');
    fChanged := True;
    if AutoSave then
      Save;
  end;
end;(**)

(*procedure TXIniFile.ClearSection(sect: KOLString);
begin
  ClearSection(SectionIndex(sect));
end;(**)

procedure TXIniFile.DeleteSection(iSection: Integer);
begin
  if (iSection > 0) and (iSection < fSections.Count) then
  begin
    PKOLStrList(fSections.Objects[iSection]).Free;
    fSections.Delete(iSection);
    fChanged := True;
    if AutoSave then
      Save;
  end;
end; (**)

(*procedure TXIniFile.ClearAll;
var
  i: Integer;
begin
  for i := 0 to SectionsCount do
    ClearSection(i);
  fChanged := True;
  if AutoSave then
    Save;
end;(**)

(*procedure TXIniFile.DeleteAll;
var
  i: Integer;
begin
  for i := 0 to SectionsCount do
    DeleteSection(i);
  fChanged := True;
  if AutoSave then
    Save;
end;(**)

(*procedure TXIniFile.RenameKey(iSection: Integer; oldName, newName: KOLString);
var
  iName: Integer;
  curData: PKOLStrList;
begin
  iName := KeyIndex(iSection, oldName);
  if iName >= 0 then
  begin
    curData := PKOLStrList(fSections.Objects[iSection]);
    oldName := curData.Items[iName];
    ParseW(oldName, '=');
    curData.Items[iName] := newName + '=' + oldName;
    fChanged := True;
    if AutoSave then
      Save;
  end;
end;(**)

(*procedure TXIniFile.RenameSection(iSection: Integer; newName: KOLString);
begin
  fSections.Items[iSection] := newName;
  fChanged := True;
  if AutoSave then
    Save;
end;(**)

procedure TXIniFile.AddSection(sect: KOLString);
var
  newSection: PKOLStrList;
  iSect: Integer;
begin
  iSect := fSections.Add(sect);
  newSection := NewWStrList;
  newSection.Add(''); //������� ������ ��������������
  fSections.Objects[iSect] := PtrUInt(newSection);
  fChanged := True;
  if AutoSave then
    Save;
end;

procedure TXIniFile.AddKey(iSection: Integer; keyName, value: KOLString);
var
  curData: PKOLStrList;
begin
  if (iSection >= 0) and (iSection < fSections.Count) then
  begin
    curData := PKOLStrList(fSections.Objects[iSection]);
    curData.Add(keyName + '=' + value);
    fChanged := True;
    if AutoSave then
      Save;
  end;
end;

procedure TXIniFile.AddKey(keyName, value: KOLString);
begin
  AddKey(fCurrentSection, keyName, value);
end;

(*procedure TXIniFile.InsertSection(iPrevSect: Integer; sect: KOLString; data: PKOLStrList);
begin
  if (iPrevSect >= 0) and (iPrevSect < fSections.Count) then
  begin
    fSections.InsertObject(iPrevSect, sect, PtrUInt(data));
  end;
end;(**)

end.

