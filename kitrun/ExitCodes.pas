unit ExitCodes;

interface

type
  TExitCode = (OK, BadParam);

procedure ExitCode(Code: TExitCode);

implementation

uses Windows, KOL;

procedure ExitCode(Code: TExitCode);
begin
  case (Code) of
    OK: ;
    BadParam: ShowMessage('ERROR: badparams');
  end;
  ExitProcess(Cardinal(Code));
end;

end.
