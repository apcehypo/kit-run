program kitrun;

{$IFDEF DEBUG}
{$APPTYPE CONSOLE}
{$ENDIF}

{$R 'RunData.res' 'RunData.rc'}

uses
  Windows,
  KOL,
  Elevating in 'Elevating.pas',
  RunData in 'RunData.pas',
  XIniFile in 'XIniFile.pas',
  Registry in 'Registry.pas',
  CommonTools in 'CommonTools.pas',
  RegModel in 'RegModel.pas',
  Context in 'Context.pas',
  ProcessTools in 'ProcessTools.pas',
  OSTools in 'OSTools.pas',
  FileTools in 'FileTools.pas';

const
  EVENTPREFIX = 'kITRUN_';

var
  dummymsg: TMsg;
  param, executeLine: KOLString;
  i, j: Integer;
  getRunDataFromParams: Boolean;
  run: PRunData;
  runFile: KOLString;
  runFileModel: PXIniFile;

  input: KOLString;
  f: PStream;
  buf: PAnsiChar;
  bufLength, actuallyRead: Cardinal;

  resourceID: TResourceHandle;
  resourceHandle: TResourceHandle;

  regData, regAllKeys, regGeneralKeys, regRestore: PRegData;
  useRegistry: Boolean;

  fsSource, fsTarget: KOLString;

  waitHandles: array of THandle;
  eventHandle: THandle;
  findHandle: THandle;
  findData: TWin32FindData;

label
  PredefinedRunFile;

begin
  //��� ���������� �������� ������ �������
  PostMessage(0, 0, 0, 0);
  PeekMessage(dummymsg, 0, 0, 0, 0);

  runFile := '';
  getRunDataFromParams := False;
  // 0.2.1) ��������� ������� ���������� �������
  if (ParamCount = 0) then
  begin // 1.1.1) ��� ����������
  end
  else
  begin // ParamCount >= 1
    if (StrSatisfy(ParamStr(1), '*.run')) then
    begin
      if (ParamCount = 1) then
      begin // 1.1.2) �������� run-�����
        runFile := ParamStr(1);
      end
      else
      begin // 1.1.4) ������ ��������� � ���������� RUN-�������
        runFile := ParamStr(1);
        executeLine := '';
        for i := 1 to ParamCount do
          executeLine := executeLine + QuotesIfHasSpaces(ParamStr(i)) + ' ';
      end;
    end
    else // ParamCount >= 1 � ������ �� *.run
    begin
      param := ParamStr(1);
      if (param <> '') then
      begin
        if (param[1] = '/') then
        begin
          if (param = '/I') then
          begin // 1.1.6) ���������� ��������� ������
            if (ParamCount = 2) then
            begin
              eventHandle := OpenEventW(EVENT_MODIFY_STATE, False, PKOLChar(EVENTPREFIX + ParamStr(2)));
              if (eventHandle <> 0) then
              begin
                SetEvent(eventHandle);
                CloseHandle(eventHandle);
              end;
              ExitCode(OK);
            end
            else
              ExitCode(BadParam);
          end
          else
          begin // 1.1.5) ��������� � ������� ����������
            getRunDataFromParams := True;
          end;
        end
        else
        begin // 1.1.3) ������ ��������� � ���������������
          executeLine := '';
          for i := 1 to ParamCount do
            executeLine := executeLine + QuotesIfHasSpaces(ParamStr(i)) + ' ';
        end;
      end
      else
        ExitCode(BadParam);
    end;
  end;
  // �������� ������� RUN-������ (������� �������� �� ���������)
  run := InitRunData;
  // ������ ��������� ������
  if (getRunDataFromParams) then
  begin
    i := 0;
    while (i < ParamCount - 1) do
    begin
      Inc(i);
      param := ParamStr(i);
      if (param = '') then
        ExitCode(BadParam);
      if (param[1] = '/') then
      begin
        for j := 2 to Length(param) do
        begin
          case (param[j]) of
            'e', 'E':
              if (not IsRunAsAdmin) then
                Elevate;
            'p':
              run.RelativePaths := Path_StartDir;
            'P':
              run.RelativePaths := Path_ExecDir;
            'w':
              run.WorkDir := Path_StartDir;
            'W':
              run.WorkDir := Path_ExecDir;
            'i':
              if (i < ParamCount - 2) then
              begin
                Inc(i);
                run.Instance := ParamStr(i);
                if run.Instance = '/' then
                  ExitCode(BadParam);
              end
              else
                ExitCode(BadParam);
            'f':
              run.Check := Check_None;
            'k':
              if (i < ParamCount - 2) then
              begin
                Inc(i);
                run.Key := ParamStr(i);
                if run.Key = '/' then
                  ExitCode(BadParam);
              end
              else
                ExitCode(BadParam);
            'r':
              run.Restore := RegAccess_Keys;
            'R':
              run.Restore := RegAccess_Hives;
            'b', 'B':
              begin
                if (i < ParamCount - 2) then
                begin
                  Inc(i);
                  run.BackupFile := ParamStr(i);
                  if (run.BackupFile = '') or (run.BackupFile[1] = '/') then
                    ExitCode(BadParam);
                  ExpandEnvVars(run.BackupFile);
                end
                else
                  ExitCode(BadParam);
                if param[j] = 'B' then
                  run.Backup := RegAccess_Hives
                else
                  run.Backup := RegAccess_Keys;
              end;
            'd':
              run.Delete := RegAccess_Keys;
            'D':
              run.Delete := RegAccess_Hives;
            's', 'S':
              begin
                if (i < ParamCount - 2) then
                begin
                  Inc(i);
                  run.SaveFile := ParamStr(i);
                  if (run.SaveFile = '') or (run.SaveFile[1] = '/') then
                    ExitCode(BadParam);
                  ExpandEnvVars(run.SaveFile);
                end
                else
                  ExitCode(BadParam);
                if param[j] = 'S' then
                  run.Save := RegAccess_Hives
                else
                  run.Save := RegAccess_Keys;
              end;
            'u':
              run.Unregister := RegAccess_Keys;
            'U':
              run.Unregister := RegAccess_Hives;
          end;
        end;
      end
      else
        Break;
    end;
  end
  else // RUN-������ �������� �� ����� ��������� �������
  begin
    if (runFile <> '') then goto PredefinedRunFile;
    // 0.2.2) ��������� ������� ������ � ����������� �����
{$IFDEF DEBUG}
    if false then
    begin
{$ENDIF}
      bufLength := 1024;
      SetLength(input, bufLength);
      f := NewExFileStream(STD_INPUT_HANDLE);
      buf := @input[1];
      actuallyRead := f.Read(buf[0], bufLength);
{$IFNDEF DEBUG}
      if (actuallyRead <> 0) then
      begin
{$ENDIF}
        while (actuallyRead = bufLength) do
        begin
          bufLength := bufLength shl 4;
          SetLength(input, bufLength);
          buf := @input[1];
          actuallyRead := f.Read(buf[actuallyRead], bufLength);
        end;
        runFileModel := CreateXIniFromCharArray(buf, bufLength);
      end
      else
      begin
        // 0.2.3) ��������� ������� run-�����
        if (runFile = '') then
          runFile := ExtractFileNameWOext(ExePath) + '.run';
        PredefinedRunFile:
        if not IsAbsolutePath(runFile) then
          runFile := ExtractFilePath(ExePath) + runFile;
        if FileExists(runFile) then
        begin
          runFileModel := OpenXIniFile(runFile);
          runFileModel.AutoSave := False;
        end
        else // 0.2.4) ��������� ������� �������
        begin
          resourceID := FindResource(HInstance, 'RUN', RT_RCDATA);
          if (resourceID <> 0) then
          begin
            resourceHandle := LoadResource(HInstance, resourceID);
            runFileModel := CreateXIniFromCharArray(PAnsiChar(LockResource(resourceHandle)), SizeofResource(HInstance, resourceHandle));
            UnLockResource(resourceHandle);
          end;
        end;
      end;
    end;
    //����� ������ ����� ������ run-������, ��� �� �� �� ��������
    if (runFileModel <> nil) then
    begin
      runFileModel.Section := 'Configuration';
      // 0.1.3) ���� ������, �������� ������������� ����������
      if EqualsNoCase(runFileModel.ValueString('Elevate', ''), 'Admin') and not IsRunAsAdmin then
        Elevate;
      // 0.1.4) �������� RUN-������ ���������
      run := GetRunData(runFileModel);
      // ���� ����������� ��������� ��������������, �������� RUN-������
      if (executeLine <> '') then
      begin
        run.ExecType := ExecType_Single;
        run.Execute := executeLine;
      end;
      run.DefineBaseDir;
{$IFDEF DEBUG}
      PrintRunData(run);
{$ENDIF}
      regData := nil;
      // �������� ������������� ������ � �������� ������: 2.2.1) Check
      useRegistry := False;
      if (run.Check = Check_None) then
      begin
        useRegistry := True;
      end
      else
        if (run.Check = Check_KeyAbsent) then
        begin
          useRegistry := not RegKeyNameExists(run.Key);
        end
        else
        begin
          if (run.Import = RegImport_None) then
          begin
            useRegistry := False;
            warn('Check= CONFLICTS WITH Import=');
          end
          else
          begin
            // �������� REG-������
            regData := ReadRegData(run);
          end;
        end;

      if not useRegistry and
        ((run.Restore <> RegAccess_None) or
        (run.Backup <> RegAccess_None) or
        (run.Delete <> RegAccess_None) or
        (run.Save <> RegAccess_None) or
        (run.Unregister <> RegAccess_None)) then useRegistry := True;
      // 0.1.5) ���� ����������� � ����������, �������� REG-������
      if useRegistry and (regData = nil) then
      begin
        regData := ReadRegData(run);
        regAllKeys := regData.CloneAndGetDataFromRegistry(False);
        regGeneralKeys := regData.CloneAndGetDataFromRegistry(True);
      end;
{$IFDEF DEBUG}
      PrintRegData(regData);
{$ENDIF}
      // 0.1.6) ���� ����������� � ����������, ��������� ������� �������� ������������� ������ �������
      case run.Restore of
        RegAccess_None: ;
        RegAccess_Keys:
          begin
            regRestore := regAllKeys;
          end;
        RegAccess_Hives:
          begin
            regRestore := regGeneralKeys;
          end;
      end;
{$IFDEF DEBUG}
      if (regAllKeys <> nil) then
      begin
        echo(#13#10 + ' ### RESTORE');
        PrintRegData(regAllKeys);
      end;
{$ENDIF}
      // 0.1.7) ���� ������, �������������� ������� �������� ������������� ������ ������� � �������� ����
      //!!!
      case run.Backup of
        RegAccess_None: ;
        RegAccess_Keys:
          begin

          end;
        RegAccess_Hives:
          begin

          end;
      end;
      // 0.1.8) ���� ������, ������� �� ������� ������������� �����
      case run.Delete of
        RegAccess_None: ;
        RegAccess_Keys:
          begin
            regData.DeleteKeysFromRegistry(False);
          end;
        RegAccess_Hives:
          begin
            regData.DeleteKeysFromRegistry(True);
          end;
      end;
      // 0.1.9) ���� ������, ������������� �������� ����� � ������
      if run.Import <> RegImport_None then
      begin
        regData.ImportKeysToRegistry;
      end;
      // 0.1.10) ��������� �������� �������� � �������/�������
      //[PreRemoveFiles]
      for i := 0 to run.PreRemoveFiles.Count - 1 do
      begin
        fsSource := run.ResolvedRelativePath(run.PreRemoveFiles.Items[i]);
        RemoveFileSource(fsSource);
      end;
      //[PreCopyFiles]
      for i := 0 to run.PreCopyFiles.Count - 1 do
      begin
        fsTarget := run.PreCopyFiles.Items[i];
        fsSource := Parse(fsTarget, '|');
        if (fsTarget = '') then
        begin
          warn('[PreCopyFiles]' + fsSource + '|?');
          Continue;
        end;
        fsTarget := run.ResolvedRelativePath(fsTarget);
        fsSource := run.ResolvedRelativePath(fsSource);
        CopyFileSourceTarget(fsSource, fsTarget);
      end;
      // 0.1.11) ���� ������, ��������� �������� ���������
      case run.ExecType of
        ExecType_None: ;
        ExecType_Single:
          begin
            SetLength(waitHandles, 1);
            waitHandles[0] := ExecuteProgram(run.Execute, run.Context);
          end;
        ExecType_ExecList:
          begin
            if (run.ExecList.Count > 0) then
            begin
              SetLength(waitHandles, run.ExecList.Count);
              for i := 0 to run.ExecList.Count - 1 do
              begin
                waitHandles[i] := ExecuteProgram(run.ExecList.Items[i], run.Context);
              end;
            end;
          end;
        ExecType_Directory:
          begin
            findHandle := FindFirstFile(PKOLChar(run.Execute + '*.*'), findData);
            if findHandle <> INVALID_HANDLE_VALUE then
            begin
              run.ExecList.Clear;
              repeat
                if (findData.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
                  run.ExecList.Add('"' + run.Execute + findData.cFileName + '"');
              until not FindNextFile(findHandle, findData);
              FindClose(findHandle);
            end;
            SetLength(waitHandles, run.ExecList.Count);
            for i := 0 to run.ExecList.Count - 1 do
            begin
              waitHandles[i] := ExecuteProgram(run.ExecList.Items[i], run.Context);
            end;
          end;
      end;
      // 0.1.12) ���� ������, ������� ���������� ���������� ��������
      case run.Wait of
        Wait_None: ;
        Wait_AnyExit:
          begin
            if (Length(waitHandles) > 0) then
              WaitForMultipleObjects(Length(waitHandles), @waitHandles[0], False, INFINITE);
          end;
        Wait_AllExit:
          begin
            if (Length(waitHandles) > 0) then
              WaitForMultipleObjects(Length(waitHandles), @waitHandles[0], True, INFINITE);
          end;
        Wait_Number:
          begin
            if (Length(waitHandles) > 0) and (run.WaitValue < Cardinal(Length(waitHandles))) then
              WaitForSingleObject(waitHandles[run.WaitValue], INFINITE)
            else
              warn('Wait=Number');
          end;
        Wait_Command:
          begin
            eventHandle := CreateEvent(nil, True, False, PKOLChar(EVENTPREFIX + run.Instance));
            if (eventHandle <> 0) then
            begin
              WaitForSingleObject(eventHandle, INFINITE);
              CloseHandle(eventHandle);
              // 2.1.8) OnCommand
              case run.OnCommand of
                OnCommand_None: ;
                OnCommand_Close:
                  begin
                    for i := 0 to Length(waitHandles) - 1 do
                    begin
                      CloseProcessByPID(GetProcessId(waitHandles[i]));
                    end;
                  end;
                OnCommand_Terminate:
                  begin
                    for i := 0 to Length(waitHandles) - 1 do
                    begin
                      CloseProcessByPID(GetProcessId(waitHandles[i]));
                      if WaitForSingleObject(waitHandles[i], 3000) = WAIT_TIMEOUT then
                      begin
                        TerminateProcess(waitHandles[i], 0);
                      end;
                    end;
                  end;
              end;
            end;
          end;
        Wait_Time:
          begin
            Sleep(run.WaitValue);
          end;
      end;
      for i := 0 to Length(waitHandles) - 1 do
      begin
        CloseHandle(waitHandles[i]);
      end;
      // 0.1.10) ��������� �������� �������� � �������/�������
      //[PostCopyFiles]
      for i := 0 to run.PostCopyFiles.Count - 1 do
      begin
        fsTarget := run.PreCopyFiles.Items[i];
        fsSource := Parse(fsTarget, '|');
        if (fsTarget = '') then
        begin
          warn('[PreCopyFiles]' + fsSource + '|?');
          Continue;
        end;
        fsTarget := run.ResolvedRelativePath(fsTarget);
        fsSource := run.ResolvedRelativePath(fsSource);
        CopyFileSourceTarget(fsSource, fsTarget);
      end;
      //[PostRemoveFiles]
      for i := 0 to run.PostRemoveFiles.Count - 1 do
      begin
        fsSource := run.ResolvedRelativePath(run.PreRemoveFiles.Items[i]);
        RemoveFileSource(fsSource);
      end;
      // 0.1.13) ���� ������, �������������� ������� �������� ���������� ������ ������� � �������� ����
      case run.Save of
        RegAccess_None: ;
        RegAccess_Keys:
          begin
            //!!!
          end;
        RegAccess_Hives:
          begin
            //!!!
          end;
      end;
      // 0.1.14) ���� ������, ������� �� ������� �������� �����
      case run.Unregister of
        RegAccess_None: ;
        RegAccess_Keys:
          begin
            regData.DeleteKeysFromRegistry(False);
          end;
        RegAccess_Hives:
          begin
            regData.DeleteKeysFromRegistry(True);
          end;
      end;
      // 0.1.15) ���� ������, ������������� � ������ �����, ����������� �����
      case run.Restore of
        RegAccess_None: ; //������ �� ������
        RegAccess_Keys, RegAccess_Hives: //����� ���� ��������� ������ ��, ��� ����
          begin
            regRestore.ImportKeysToRegistry;
          end;
      end;
      // 0.1.16) �����������
    end
    else
      ExitCode(NoRunData);

{$IFDEF DEBUG}
    echo('DONE');
    readln;
{$ENDIF}
    ExitCode(OK);
  end.

