unit Elevating;

interface

uses Windows, ShellApi, KOL, CommonTools;

procedure Elevate;
function RunAsAdmin(hWnd: hWnd; Filename: KOLString; Parameters: KOLString): Boolean;
function IsRunAsAdmin(): BOOL;

implementation

procedure Elevate;
var
  i: Integer;
  params: KOLString;
begin
  //!!!
  params := '';
  for i := 1 to ParamCount do
  begin
    params := params + QuotesIfHasSpaces(ParamStr(i)) + ' ';
  end;
  if (RunAsAdmin(0, ExePath, params)) then
    ExitCode(OK)
  else
    ExitCode(ElevationFailure);
end;

const
  SECURITY_NT_AUTHORITY: TSIDIdentifierAuthority = (Value: (0, 0, 0, 0, 0, 5));

const
  SECURITY_BUILTIN_DOMAIN_RID = $00000020;
  DOMAIN_ALIAS_RID_ADMINS = $00000220;

function RunAsAdmin(hWnd: hWnd; Filename: KOLString; Parameters: KOLString): Boolean;
{ http://msdn.microsoft.com/en-us/library/bb756922.aspx }
var
  sei: TShellExecuteInfoW;
begin
  ZeroMemory(@sei, SizeOf(sei));
  sei.cbSize := SizeOf(TShellExecuteInfo);
  sei.Wnd := hWnd;
  sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI;
  sei.lpVerb := 'runas';
  sei.lpFile := PKOLChar(Filename); // PAnsiChar;
  if (Parameters <> '') then
    sei.lpParameters := PKOLChar(Parameters); // PAnsiChar;
  sei.nShow := SW_SHOWNORMAL; // Integer;
  Result := ShellExecuteExW(@sei);
end;

//
// FUNCTION: IsRunAsAdmin()
//
// PURPOSE: The function checks whether the current process is run as
// administrator. In other words, it dictates whether the primary access
// token of the process belongs to user account that is a member of the
// local Administrators group and it is elevated.
//
// RETURN VALUE: Returns TRUE if the primary access token of the process
// belongs to user account that is a member of the local Administrators
// group and it is elevated. Returns FALSE if the token does not.
//
// EXCEPTION: If this function fails, it throws a C++ DWORD exception which
// contains the Win32 error code of the failure.
//
// EXAMPLE CALL:
// try
// {
// if (IsRunAsAdmin())
// wprintf (L"Process is run as administrator\n");
// else
// wprintf (L"Process is not run as administrator\n");
// }
// catch (DWORD dwError)
// {
// wprintf(L"IsRunAsAdmin failed w/err %lu\n", dwError);
// }
//

function CheckTokenMembership(TokenHandle: THandle; SidToCheck: PSID; var IsMember: BOOL): BOOL; stdcall; external advapi32;

function IsRunAsAdmin(): BOOL;
label
  Cleanup;
var
  fIsRunAsAdmin: BOOL;
  dwError: DWORD;
  pAdministratorsGroup: PSID;
  NtAuthority: SID_IDENTIFIER_AUTHORITY;
begin
  fIsRunAsAdmin := False;
  dwError := ERROR_SUCCESS;
  pAdministratorsGroup := nil;

  // Allocate and initialize a SID of the administrators group.
  NtAuthority := SECURITY_NT_AUTHORITY;
  if (not AllocateAndInitializeSid(NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, pAdministratorsGroup)) then
  begin
    dwError := GetLastError();
    goto Cleanup;
  end;

  // Determine whether the SID of administrators group is enabled in
  // the primary access token of the process.
  if (not CheckTokenMembership(0, pAdministratorsGroup, fIsRunAsAdmin)) then
  begin
    dwError := GetLastError();
    goto Cleanup;
  end;

Cleanup:
  // Centralized cleanup for all allocated resources.
  if (pAdministratorsGroup <> nil) then
  begin
    FreeSid(pAdministratorsGroup);
    pAdministratorsGroup := nil;
  end;

  // Throw the error if something failed in the function.
  if (dwError = ERROR_SUCCESS) then
  begin
    Result := fIsRunAsAdmin;
  end
  else
  begin
    Result := False;
  end;

end;

end.
