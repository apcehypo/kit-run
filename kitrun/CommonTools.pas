unit CommonTools;

interface

uses Windows, KOL;

type
  TExitCodes = (OK, NoRunData, BadParam, ElevationFailure);

procedure ExitCode(Code: TExitCodes);
procedure echo(text: KOLString);
procedure warn(text: KOLString);
function QuotesIfHasSpaces(Str: KOLString): KOLString;
function IsAbsolutePath(const Path: KOLString): Boolean;
function IsDigit(char: KOLChar): Boolean;
procedure SplitCommandLine(cmdLine: KOLString; var progName: KOLString; var params: KOLString);
function EqualsNoCase(S1, S2: KOLString): Boolean;
function DuplicateChar(S: KOLString; C: KOLChar): KOLString;

implementation

procedure ExitCode(Code: TExitCodes);
begin
  case (Code) of
    OK:
      ;
    NoRunData:
      echo('ERROR: no RUN data');
    BadParam:
      echo('ERROR: bad params');
    ElevationFailure:
      echo('ERROR: elevation failed');
  end;
{$IFDEF DEBUG}
  Readln;
{$ENDIF}
  ExitProcess(Cardinal(Code));
end;

procedure echo(text: KOLString);
begin
{$IFDEF DEBUG}
  writeln(text);
{$ELSE}
  ShowMessage(text);
{$ENDIF}
end;

procedure warn(text: KOLString);
begin
  echo('BAD VALUE: ' + text);
end;

function QuotesIfHasSpaces(Str: KOLString): KOLString;
begin
  if IndexOfChar(Str, ' ') > 0 then
    Result := '"' + Str + '"'
  else
    Result := Str;
end;

function IsAbsolutePath(const Path: KOLString): Boolean;
begin
  Result := (copy(Path, 1, 2) = '\\') or (copy(Path, 2, 2) = ':\');
end;

function IsDigit(char: KOLChar): Boolean;
begin
  case (char) of
    '0'..'9': Result := True;
  else
    Result := False;
  end;
end;

procedure SplitCommandLine(cmdLine: KOLString; var progName: KOLString; var params: KOLString);
begin
  if (cmdLine <> '') then
  begin
    if (cmdLine[1] = '"') then
    begin
      Parse(cmdLine, '"');
      progName := Parse(cmdLine, '"');
    end
    else
    begin
      progName := Parse(cmdLine, ' ');
    end;
    params := cmdLine;
  end
  else
  begin
    progName := '';
    params := '';
  end;
end;

function EqualsNoCase(S1, S2: KOLString): Boolean;
begin
  S1 := UpperCase(S1);
  S2 := UpperCase(S2);
  Result := S1 = S2;
end;

function DuplicateChar(S: KOLString; C: KOLChar): KOLString;
var
  iSrc, iRes, lenSrc: Integer;
begin
  lenSrc := Length(S);
  SetLength(Result, lenSrc * 2);
  iRes := 1;
  for iSrc := 1 to lenSrc do
  begin
    Result[iRes] := S[iSrc];
    Inc(iRes);
    if (S[iSrc] = C) then
    begin
      Result[iRes] := C;
      Inc(iRes);
    end;
  end;
  SetLength(Result, iRes - 1);
end;

end.

