unit OSTools;

interface

uses Windows, KOL;

procedure ExpandEnvVars(var Path: KOLString);
function IsWOW64: Boolean;

implementation

procedure ExpandEnvVars(var Path: KOLString);
var
  Buffer: KOLString;
begin
  if IndexOfStr(Path, '%') > 0 then
  begin
    SetLength(Buffer, $FF * SizeOf(KOLChar));
    SetLength(Buffer, ExpandEnvironmentStrings(PKOLChar(Path), @Buffer[1],
      $FF) - 1);
    Path := Buffer;
    SetLength(Buffer, 0);
  end;
end;

function IsWOW64: Boolean;
type
  TIsWow64Process = function(
    Handle: THandle;
    var Res: BOOL
  ): BOOL; stdcall;
var
  IsWow64Result: BOOL;
  IsWow64Process: TIsWow64Process;
begin
  IsWow64Process := GetProcAddress(
    GetModuleHandle('kernel32'), 'IsWow64Process'
  );
  if Assigned(IsWow64Process) then
  begin
    IsWow64Process(GetCurrentProcess, IsWow64Result);
    Result := IsWow64Result;
  end
  else
    Result := False;
end;

{function UserName: KOLString;
var
  nSize: DWord;
begin
 nSize := 1024;
 SetLength(Result, nSize);
 if GetUserName(PKOLChar(Result), nSize) then
   SetLength(Result, nSize-1);
end;{}

end.
