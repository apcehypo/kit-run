{$DEFINE UNICODE_CTRLS}
{$I KOLDEF.inc}
unit Context;

interface

uses KOL, XIniFile, CommonTools;

type
  PContext = ^TContext;
  TContext = object(TObj)
  private
    _vars: PXIniFile;
    function GetVar(const name: KOLString): KOLString;
    procedure SetVar(const name: KOLString; value: KOLString);
    function GetEnv(const name: KOLString): KOLString;
    procedure SetEnv(const name: KOLString; value: KOLString);
  public
    BaseDir: KOLString;
    property Vars[const name: KOLString]: KOLString read GetVar write SetVar;
    property Envs[const name: KOLString]: KOLString read GetEnv write SetEnv;
    procedure Resolve(var value: KOLString);
  end;

function InitContext: PContext;

implementation

function InitContext: PContext;
begin
  New(Result, Create);
{$IFDEF DEBUG_OBJKIND}
  Result.fObjKind := 'TContext';
{$ENDIF}
  Result._vars := CreateEmptyXIniFile;
end;

function TContext.GetVar(const name: KOLString): KOLString;
begin
  Result := Self._vars.ValueString(name, '');
end;

procedure TContext.SetVar(const name: KOLString; value: KOLString);
var
  curValue: KOLString;
begin
  curValue := Self._vars.ValueString(name, #13);
  if (curValue = #13) then
  begin
    Self._vars.AddKey(name, value);
  end
  else
  begin
    Self._vars.SetValueString(name, value);
  end;
end;

function TContext.GetEnv(const name: KOLString): KOLString;
var
  bufSize: Cardinal;
begin
  bufSize := GetEnvironmentVariable(PKOLChar(name), nil, 0);
  if (bufSize > 0) then
  begin
    SetLength(Result, bufSize - 1);
    GetEnvironmentVariable(PKOLChar(name), PKOLChar(Result), bufSize);
  end
  else
    Result := '';
end;

procedure TContext.SetEnv(const name: KOLString; value: KOLString);
begin
  SetEnvironmentVariable(PKOLChar(name), PKOLChar(value));
end;

procedure TContext.Resolve(var value: KOLString);
var
  source, result, expr: KOLString;

  function ResolvedExpr(expr: KOLString): KOLString;
  var
    duplicateSlashes: Boolean;
    len: Integer;
  begin
    if (expr <> '') then
    begin
      duplicateSlashes := False;
      len := Length(expr);
      if (expr[len] = '\') then
      begin
        duplicateSlashes := True;
        expr := Copy(expr, 1, len - 1);
      end;
      if (expr[1] = '$') then
      begin
        Parse(expr, '$');
        Result := Self.Vars[expr];
      end
      else
      begin
        Result := Self.Envs[expr];
      end;
      if duplicateSlashes then
      begin
        Result := DuplicateChar(Result, '\');
      end;  
    end;
  end;

begin
  source := value;
  result := '';
  while (IndexOfStr(source, '`') > 0) do
  begin
    result := result + Parse(source, '`');
    expr := Parse(source, '`');
    result := result + ResolvedExpr(expr);
  end;
  result := result + source;
  value := result;
end;

end.

