unit ProcessTools;

interface
uses Windows, ShellAPI, KOL, CommonTools, Context;

function ExecuteProgram(cmdLine: KOLString; context: PContext): THandle;
procedure CloseProcessByPID(PID: DWORD);
function GetProcessId(Process: THandle): DWORD; stdcall; external 'kernel32.dll' name 'GetProcessId';

implementation

function ExecuteProgram(cmdLine: KOLString; context: PContext): THandle;
var
  execInfo: SHELLEXECUTEINFOW;
  name, checkName, params: KOLString;
begin
  SplitCommandLine(cmdLine, name, params);
  if not IsAbsolutePath(name) then
  begin
    checkName := context.BaseDir + name;
    if FileExists(checkName) then name := checkName;
  end;

  ZeroMemory(@execInfo, SizeOf(SHELLEXECUTEINFOW));
  execInfo.cbSize := SizeOf(SHELLEXECUTEINFOW);
  execInfo.fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_NO_UI;
  execInfo.lpFile := PKOLChar(name);
  execInfo.lpParameters := PKOLChar(params);
  execInfo.nShow := SW_SHOWNORMAL;
  execInfo.lpDirectory := PKOLChar(context.Vars['WorkDir']);
  ShellExecuteExW(@execInfo);
  Result := execInfo.hProcess;
end;

procedure CloseProcessByPID(PID: DWORD);
  function EnumWindowsProc(hwnd: HWND; lParam: LPARAM): Boolean; stdcall;
  var
    PID: DWORD;
    Res: DWORD;
  begin
    GetWindowThreadProcessId(hwnd, @PID);
    if PID = Cardinal(lParam) then
    begin
      SendMessageTimeout(hwnd, 16, 0, 0, SMTO_NORMAL, 100, Res);
    end;
    Result := True;
  end;
begin
  EnumWindows(@EnumWindowsProc, PID);
end;

end.

