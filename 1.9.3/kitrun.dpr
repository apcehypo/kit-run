program kitrun;

{$IFDEF DEBUG}
{$APPTYPE CONSOLE}
{$ENDIF}

{$R kitrun.res}
{$R version.res}

uses
  Windows, ShellAPI,
  KOL,
  registry in 'registry.pas';

var
  FORCE,
    UNREG, UNREGFULL,
    BACKUPFULL, SAVEFULL,
    RESTORE, RESTOREFULL,
    DELREG, DELREGFULL,
    STOREKEYS: Boolean;
  BaseDir, WorkDir: KOLString;
  param, subparam: KOLString;
  KValue, BValue, SValue: KOLString;
  RegFileName: KOLString;
  Prog, Params: KOLString;
  i, j: Integer;
  RegKey: KOLString;
  RegHive, KHive: KOLString;
  RegAsIni: PIniFile;
  Sections, AllImportedKeys, GenImportedKeys: PKOLStrList;
  execinfo: SHELLEXECUTEINFOW;
  dummymsg: TMsg;

  MainRegFileKeys, BackupRegFileKeys: PArrayOfKOLStrList;

{$IFDEF DEBUG}

procedure PrintDebug();
begin
  Writeln('FORCE: ', FORCE);
  Write('DELREG: ', DELREG);
  Writeln('  DELREGFULL: ', DELREGFULL);
  Write('UNREG: ', UNREG);
  Writeln('  UNREGFULL: ', UNREGFULL);
  Write('RESTORE: ', RESTORE);
  Writeln('  RESTOREFULL: ', RESTOREFULL);
  Writeln('KValue: ', KValue);
  Write('BValue: ', BValue);
  Writeln('  BACKUPFULL: ', BACKUPFULL);
  Write('SValue: ', SValue);
  Writeln('  SAVEFULL: ', SAVEFULL);
  Writeln('STOREKEYS: ', STOREKEYS);
  Writeln('RegFileName: ', RegFileName);
  Write('Prog: ' + Params);
  Writeln('  Params: ' + Params);
  Writeln('BaseDir: ' + BaseDir);
  Writeln('WorkDir: ' + WorkDir);
end;

procedure PrintArrayOfKeys(Keys: PArrayOfKOLStrList);
var
  i, j: Integer;
begin
  if (Keys <> nil) then
  begin
    for i := 0 to Length(Keys^) - 1 do
    begin
      if Keys^[i] <> nil then
      begin
        Writeln('[', Keys^[i].Items[0], ']');
        for j := 1 to Keys^[i].Count - 1 do
          Writeln(Keys^[i].Items[j]);
      end;
    end;
  end;
end;
{$ENDIF}

procedure ShowHelpAndExit();
var
  readme: KOLString;
  platform: KOLString;
begin
  readme := GetStartDir + 'readme.txt';
  if FileExists(readme) then
  begin
    ShellExecuteW(0, '', 'notepad.exe', PKOLChar(readme), PKOLChar(GetStartDir), SW_SHOWNOACTIVATE);
    Sleep(200);
  end;
  platform := {$IFDEF WIN64}'x64'{$ELSE}'x86'{$ENDIF};
  ShowMessage('kIT Portable Launcher 1.9 UNICODE'#9+platform+#9'FREEWARE'#13#10'ApceH Hypocrite � 2011�2013'#13#10#13#10'Usage:'#13#10 +
    'kitrun.exe  [/f]  [/p|/P]  [/w|/W]  [/d|/D]  [/u|/U]  [/r|/R]'#13#10#9 +
    '[/b|/B (:|file)]  [/s|/S (:|file)]  [/k registry_key]'#13#10#9 +
    'reg-file  command [param]...'#13#10#13#10 +
    'You can combine an options into the single word.');
  ExitProcess(1);
end;

function IsAbsolutePath(const Path: KOLString): Boolean;
begin
  Result := (copy(path, 1, 2) = '\\') or (copy(path, 2, 2) = ':\');
end;

procedure ExpandEnvVars(var Path: KOLString);
var
  Buffer: KOLString;
begin
  if IndexOfStr(Path, '%') > 0 then
  begin
    SetLength(Buffer, $FF * SizeOf(KOLChar));
    SetLength(Buffer, ExpandEnvironmentStrings(PKOLChar(Path), @Buffer[1], $FF) - 1);
    Path := Buffer;
    SetLength(Buffer, 0);
  end;
end;

function GetRegFileFirstSection(FileName: KOLString): KOLString;
var
  RegAsIni: PIniFile;
  SectionNames: PKOLStrList;
begin
  if FileExists(FileName) then
  begin
    RegAsIni := OpenIniFile(FileName);
    RegAsIni.Mode := ifmRead;
    SectionNames := NewKOLStrList;
    RegAsIni.GetSectionNames(SectionNames);
    RegAsIni.Free;
    if SectionNames.Count > 0 then
    begin
      Result := SectionNames.Items[0];
      SectionNames.Free;
    end
    else
      Result := '';
  end
  else
    Result := '';
end;

begin
  //��� ���������� �������� ������ �������
  PostMessage(0, 0, 0, 0);
  PeekMessage(dummymsg, 0, 0, 0, 0);

  FORCE := False;
  UNREG := False;
  UNREGFULL := False;
  BACKUPFULL := False;
  SAVEFULL := False;
  RESTORE := False;
  RESTOREFULL := False;
  DELREG := False;
  DELREGFULL := False;

  if ParamCount >= 2 then
  begin
    i := 0;
    while i < ParamCount - 1 do
    begin
      Inc(i);
      param := ParamStr(i);
      if param[1] = '/' then
      begin
        for j := 2 to Length(param) do
        begin
          if param[j] = 'f' then
          begin
            FORCE := True;
            Continue;
          end;
          if param[j] = 'u' then
          begin
            if UNREGFULL then
              ShowHelpAndExit; //���������������� � /U
            UNREG := True;
            Continue;
          end;
          if param[j] = 'U' then
          begin
            if UNREG then
              ShowHelpAndExit; //���������������� � /u
            UNREG := True;
            UNREGFULL := True;
            Continue;
          end;
          if param[j] = 'p' then
          begin
            if BaseDir <> '' then
              ShowHelpAndExit; //���������������� � /P
            BaseDir := GetStartDir;
            Continue;
          end;
          if param[j] = 'P' then
          begin
            if BaseDir <> '' then
              ShowHelpAndExit; //���������������� � /p
            BaseDir := '?';
            Continue;
          end;
          if param[j] = 'w' then
          begin
            if WorkDir <> '' then
              ShowHelpAndExit; //���������������� � /W
            WorkDir := GetStartDir;
            Continue;
          end;
          if param[j] = 'W' then
          begin
            if WorkDir <> '' then
              ShowHelpAndExit; //���������������� � /w
            WorkDir := '?';
            Continue;
          end;
          if param[j] = 'd' then
          begin
            if DELREGFULL then
              ShowHelpAndExit; //���������������� � /D
            DELREG := True;
            Continue;
          end;
          if param[j] = 'D' then
          begin
            if DELREG then
              ShowHelpAndExit; //���������������� � /d
            DELREG := True;
            DELREGFULL := True;
            Continue;
          end;
          if param[j] = 'r' then
          begin
            if RESTOREFULL then
              ShowHelpAndExit; //���������������� � /R
            RESTORE := True;
            Continue;
          end;
          if param[j] = 'R' then
          begin
            if RESTORE then
              ShowHelpAndExit; //���������������� � /r
            RESTORE := True;
            RESTOREFULL := True;
            Continue;
          end;
          if param[j] = 'k' then
          begin
            if (i < ParamCount - 2) then
            begin
              Inc(i);
              KValue := ParamStr(i);
              if KValue[1] = '/' then
                ShowHelpAndExit;
            end
            else
              ShowHelpAndExit;
            Continue;
          end;
          if (param[j] = 's') or (param[j] = 'S') then
          begin
            if (i < ParamCount - 2) then
            begin
              Inc(i);
              SValue := ParamStr(i);
              if SValue[1] = '/' then
                ShowHelpAndExit;
              ExpandEnvVars(SValue);
            end
            else
              ShowHelpAndExit;
            if param[j] = 'S' then
              SAVEFULL := True;
            Continue;
          end;
          if (param[j] = 'b') or (param[j] = 'B') then
          begin
            if (i < ParamCount - 2) then
            begin
              Inc(i);
              BValue := ParamStr(i);
              if BValue[1] = '/' then
                ShowHelpAndExit;
              ExpandEnvVars(BValue);
            end
            else
              ShowHelpAndExit;
            if param[j] = 'B' then
              BACKUPFULL := True;
            Continue;
          end;
        end;
      end
      else
        Break;
    end;
    ExpandEnvVars(param);
    RegFileName := param;
    Inc(i);
    //���� ����������� ���������
    Prog := ParamStr(i);
    ExpandEnvVars(Prog);

    if BaseDir = '' then
      BaseDir := GetWorkDir
    else if BaseDir = '?' then
    begin
      if IsAbsolutePath(Prog) then
        BaseDir := ExtractFilePath(Prog)
      else
        BaseDir := GetWorkDir; //������������� /P
    end;

    //���������� ������������� �����
    if not IsAbsolutePath(RegFileName) then
      RegFileName := BaseDir + RegFileName;
    if (not FileExists(RegFileName)) or (not IsRegFile(RegFileName)) then
      ExitProcess(3);

    //���������� ������������� �����
    if not IsAbsolutePath(Prog) then
      Prog := BaseDir + Prog;

    if WorkDir = '' then
      WorkDir := GetWorkDir
    else if WorkDir = '?' then
    begin
      if IsAbsolutePath(Prog) then
        WorkDir := ExtractFilePath(Prog)
      else
        WorkDir := GetWorkDir; //������������� /W
    end;

    //��������� ����������� ���������
    Inc(i);
    Params := '';
    while i <= ParamCount do
    begin
      subparam := ParamStr(i);
      AddQuotesIfHasSpaces(subparam);
      Params := Params + ' ' + subparam;
      Inc(i);
    end;
    ExpandEnvVars(Params);

    if (BValue <> '') then
      if (BValue = ':') then
        BValue := RegFileName
      else if not IsAbsolutePath(BValue) then
        BValue := BaseDir + BValue;

    if (SValue <> '') then
      if (SValue = ':') then
        SValue := RegFileName
      else if not IsAbsolutePath(SValue) then
        SValue := BaseDir + SValue;

    RegKey := GetRegFileFirstSection(RegFileName);
    if RegKey = '' then
      ExitProcess(2);
    RegHive := Parse(RegKey, '\');

    STOREKEYS := UNREG or RESTORE or (BValue <> '') or (SValue <> '');

    if KValue = '' then
    begin
      KValue := RegKey;
      KHive := RegHive;
    end
    else
      KHive := Parse(KValue, '\');

    if not FORCE then
    begin
      if not RegKeyExists(Str2RegHive(KHive), KValue) then
        FORCE := True; //������, ������ ��� �� �������������
    end;

    //����������� ������������� ������, ���� ����������
    if STOREKEYS then
    begin
      AllImportedKeys := NewKOLStrList;
      GenImportedKeys := NewKOLStrList;

      RegAsIni := OpenIniFile(RegFileName);
      RegAsIni.Mode := ifmRead;
      Sections := NewKOLStrList;
      RegAsIni.GetSectionNames(Sections);
      for i := 0 to Sections.Count - 1 do
        MergeKeyList(AllImportedKeys, GenImportedKeys, Sections.Items[i]);
      Sections.Free;
      RegAsIni.Free;
    end;

{$IFDEF DEBUG}
  Writeln(RegFileName);
{$ENDIF}
    //������ �������� reg-����
    MainRegFileKeys := RegFile2KeysList(RegFileName);
{$IFDEF DEBUG}
  Readln;
{$ENDIF}

{$IFDEF DEBUG}
    Writeln('>> MainRegFileKeys:');
    PrintArrayOfKeys(MainRegFileKeys);
    Writeln('>>=================');
    PrintDebug;
    Readln;
//    ExitProcess(0);
{$ENDIF}

    //���������� ������� ���������� ����� � ������
    if RESTOREFULL then
    begin
      BackupRegFileKeys := ExportFullKeys(GenImportedKeys);
    end
    else if RESTORE then
    begin
      BackupRegFileKeys := ExportKeys(AllImportedKeys);
    end;

{$IFDEF DEBUG}
    Writeln('>> BackupRegFileKeys:');
    if RESTORE then
      PrintArrayOfKeys(BackupRegFileKeys);
    Writeln('>>=================');
{$ENDIF}

    //backup
    if BValue <> '' then
    begin
      if BACKUPFULL then
      begin
        if RESTOREFULL then
          KeysList2RegFile(BackupRegFileKeys, BValue)
        else
          KeysList2RegFile(ExportFullKeys(GenImportedKeys), BValue)
      end
      else
      begin
        if RESTORE and not RESTOREFULL then
          KeysList2RegFile(BackupRegFileKeys, BValue)
        else
          KeysList2RegFile(ExportKeys(AllImportedKeys), BValue);
      end;
    end;

    //�������, ���� /d ��� /D
    if DELREGFULL then
    begin
      for i := 0 to GenImportedKeys.Count - 1 do
      begin
        RegKey := GenImportedKeys.Items[i];
        RegKeyFullDelete(Str2RegHive(Parse(RegKey, '\')), PKOLChar(RegKey));
      end;
    end
    else if DELREG then
    begin
      for i := AllImportedKeys.Count - 1 downto 0 do
      begin
        RegKey := AllImportedKeys.Items[i];
        RegKeyDelete(Str2RegHive(Parse(RegKey, '\')), PKOLChar(RegKey));
      end;
    end;

    //������
    if FORCE then
    begin
      ImportKeys(MainRegFileKeys);
      FreeArrayOfKOLStrList(MainRegFileKeys); //������ �� �����������
    end;

    //������ ���������
    ZeroMemory(@execinfo, SizeOf(SHELLEXECUTEINFOW));
    execinfo.cbSize := SizeOf(SHELLEXECUTEINFOW);
    execinfo.fMask := SEE_MASK_NOCLOSEPROCESS;
    execinfo.lpFile := PKOLChar(Prog);
    execinfo.lpParameters := PKOLChar(Params);
    execinfo.nShow := SW_SHOWNORMAL;
    execinfo.lpDirectory := PKOLChar(WorkDir);
    ShellExecuteExW(@execinfo);

    if FORCE and STOREKEYS then
    begin
      //���������� ����������
      WaitForSingleObject(execinfo.hProcess, INFINITE);
      //���������� � ������������ � /s ��� /S
      if SValue <> '' then
      begin
        if SAVEFULL then
        begin
          KeysList2RegFile(ExportFullKeys(GenImportedKeys), SValue);
        end
        else
        begin
          KeysList2RegFile(ExportKeys(AllImportedKeys), SValue);
        end;
      end;

      if UNREGFULL then
      begin
        //�������� ���������� �����
        for i := 0 to GenImportedKeys.Count - 1 do
        begin
          RegKey := GenImportedKeys.Items[i];
          RegKeyFullDelete(Str2RegHive(Parse(RegKey, '\')), PKOLChar(RegKey));
        end;
      end
      else if UNREG then
      begin
        //�������� ������ �������� ������
        for i := AllImportedKeys.Count - 1 downto 0 do
        begin
          RegKey := AllImportedKeys.Items[i];
          RegKeyDelete(Str2RegHive(Parse(RegKey, '\')), PKOLChar(RegKey));
        end;
      end;

      //��������������� � ������������ � /r ��� /R
      if RESTORE then
      begin
        ImportKeys(BackupRegFileKeys);
      end;

    end;
  end
  else
    ShowHelpAndExit;
{$IFDEF DEBUG}
  Readln;
{$ENDIF}
end.

